# -*- coding: utf-8 -*-
import json,codecs
import csv
import datetime
from XMLFirst import XMLWikipedia
import copy as cp

class MachineLearning(object):
    """ Ogólna klasa dla wszystkich class machine lerningu. Wczytywane oraz zapisywane są cechy do plików"""


    def _separeteArticlesMultiCategory(self):
        XMLWiki = XMLWikipedia(self.folder)
        XMLWiki.loadCategoriesTable()
        for category in self.categories:  # trzeba wykonowyac przed kazdym nauczaniem (z powodu mozliwosci artykulow wystepujacych w kilku kategoriach, a robimy tylko rozlaczne)
            print self.subfolder + '/' + category+self.oneAcapitFilename + '.xml'
            XMLWiki.readXML(self.subfolder + '/' + category+self.oneAcapitFilename + '.xml')
            XMLWiki.export(['wordsNumberCSV'], category, onlySeparableCats=self.onlySeparableCats, categoriesArr=cp.copy(self.categories))
        return XMLWiki.idArraySepareteArticle

    def prepereFeatures(self, categories, filenameAdd2):
        """
        :param categories: tablca - kategorii przy nauczaniu np. [u'biologia komórki', u'biologia molekularna', u'biologia rozrodu']
        :param filenameAdd2: string - dodatek do zapisywanych plików
        :help self.extendedCSV: boolean - czy ma zostac zapisany plik csv ze slowami z procentami (rozbudowany) 
        :return: zapisuje do zmiennej:
         self.cechyArray tablicę cech
         self.filenameFeatures nazwe pliku z cechami
        """

        dateNow = datetime.datetime.now()
        filenameAdd = str(dateNow.year)+'-'+str(dateNow.month)+'-'+str(dateNow.day)+"_"+str(dateNow.hour)+'-'+str(dateNow.minute)+'-'+str(dateNow.second)
        print "prepereFeatures"
        self.dictFeatures = {}
        for category in categories:
            with open(u"resources/Words"+category+".csv", 'r') as outfile:
                reader = csv.reader(outfile, delimiter=';')
                next(reader)  # peirwszy wersz  to nazwa kolumny (omijamy)
                for row in reader:
                    row[0] = unicode(row[0], 'cp1250')
                    try:
                        self.dictFeatures[row[0]][category] = int(row[1])
                        self.dictFeatures[row[0]]['all'] += int(row[1])
                    except:
                        self.dictFeatures[row[0]] = dict()
                        self.dictFeatures[row[0]][category] = int(row[1])
                        self.dictFeatures[row[0]]['all'] = int(row[1])

        f = codecs.open("resources/allWords"+filenameAdd2+"_"+filenameAdd+".csv", encoding='cp1250', mode="wb", errors='ignore')
        if self.extendedCSV:
            f.write(b'Slowo;kategoria;ilosc;Procent ze wszystkich;kategoria;ilosc;...;\n')
        else:
            f.write(b'Slowo;\n')
        # writer = csv.writer(f)
        index = -1
        rowsCSV = []
        for word in sorted(self.dictFeatures):
            if self.dictFeatures[word]['all'] <= self.minAppearFeature:
                continue
            index += 1
            rowsCSV.append([word])
            if self.extendedCSV:
                for category in self.dictFeatures[word]:
                    if category == 'all':
                        continue
                    # print word, self.dictFeatures[word], self.dictFeatures[word][category]

                    procenty = round(self.dictFeatures[word][category] / float(self.dictFeatures[word]['all']), 4) * 100
                    rowsCSV[index].extend([category, self.dictFeatures[word][category], str(procenty).replace('.', ',')])

                for i in range((len(rowsCSV[index]) - 1) / 3):
                    for j in range((len(rowsCSV[index]) - 1) / 3 - 1):
                        if float( rowsCSV[index][j * 3 + 3].replace(',', '.')) < float( rowsCSV[index][i * 3 + 3].replace(',', '.')):
                            rowsCSV[index][j * 3 + 3],  rowsCSV[index][i * 3 + 3] = str( rowsCSV[index][i * 3 + 3]).replace('.', ','),  rowsCSV[index][j * 3 + 3].replace('.', ',')
                            rowsCSV[index][j * 3 + 1],  rowsCSV[index][i * 3 + 1] = rowsCSV[index][i * 3 + 1],  rowsCSV[index][j * 3 + 1]
                            rowsCSV[index][j * 3 + 2],  rowsCSV[index][i * 3 + 2] = rowsCSV[index][i * 3 + 2],  rowsCSV[index][j * 3 + 2]

            for cell in rowsCSV[index]:
                f.write(unicode(cell)+';')
            f.write('\n')
        f.close()
        self.cechyArray = self.__column(rowsCSV, 0)
        with open('resources/features_'+filenameAdd2+"_"+filenameAdd+'.txt', 'w+') as outfile:
            json.dump(self.cechyArray, outfile)
        self.filenameFeatures = 'features_'+filenameAdd2+"_"+filenameAdd+'.txt'

    def importFeatures(self,filename):
        """
        :param filename: string, nazwa pliku jaka ma zostac odczytana cech
        :return: zapisuje do zmiennej self.cechyArray odczytane cechy
        """
        if len(filename)>25:
            with open(filename, 'r') as outfile:
                self.cechyArray = json.load(outfile)
        else:
            with open('resources/'+filename, 'r') as outfile:
                self.cechyArray = json.load(outfile)

    def __column(self, matrix, i):
        return [row[i] for row in matrix]