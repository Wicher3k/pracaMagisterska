# -*- coding: utf-8 -*-
from classArticle import Article
from classLematization import LematizationClass
from classMachineLearning import MachineLearning
from sklearn.naive_bayes import GaussianNB
from sklearn.calibration import CalibratedClassifierCV, calibration_curve
from sklearn.externals import joblib
from classesXMLLearning import XMLTokenizationIntegerWords

from classXMLModelPreparation import XMLModelPreparation

from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from classGensim import GensimClass
from f1metricNN import f1NN,precisionNN,recallNN
import numpy as np

from keras.models import Sequential
from keras.layers import Dense
from classesXMLLearning import XMLTokenizationBooleanWords
import json

class MachineLearningMain(MachineLearning):

    def __init__(self, categories, baseFile, filenameFeatures="", extendedCSV=True, folder='', model='', onlySeparableCats=True, minAppearFeature=0, oneAcapit=False, rC="int"):
        """
        :param categories: które kategorie zostana uzyte do nauki
        :param baseFile: w jakiej nazwie plik w zostanie zapisane
        :param filenameFeatures: używane tylko do worku aby stworzyć bazę cech
        :param extendedCSV: czy raport ilości słów ma byc rozszerzony
        :param folder: folder w ktory sa wszystkei dane
        :param model: (podfolder) - które dane sa wykorzystywane
        """
        self.clf = ''
        self.categories = categories
        self.dictFeatures = {}
        self.cechyArray = {}
        self.baseFile = baseFile
        self.filenameFeatures = filenameFeatures
        self.extendedCSV = extendedCSV
        self.subfolder = model
        self.folder = folder
        self.onlySeparableCats = onlySeparableCats
        self.minAppearFeature = minAppearFeature
        self.oneAcapit = oneAcapit
        self.rC = rC
        if oneAcapit:
            self.oneAcapitFilename = "_oneAcapit"
        else:
            self.oneAcapitFilename = ""
        if self.subfolder in ('word2vec'):
            self.gensimClass = GensimClass(folder=self.folder, mode=self.subfolder, categories=self.categories,
                                           oneAcapit=self.oneAcapit, minAppearFeature=minAppearFeature, size=int(self.rC))
            self.w2vmodel = None

    def __column(self, matrix, i):
        return [row[i] for row in matrix]

    def __prepereFeatures(self):
        super(MachineLearningMain, self).prepereFeatures(self.categories, "NaiveBayes") #przygotowanie do nauczania

    def importFeatures(self):
        super(MachineLearningMain, self).importFeatures(self.filenameFeatures)



    def __fit(self,npFeaturesArray, npCategoriesArrayNN, npCategoriesArray,klasyfikatorId):
        # cechy = np.array([['a', 'alas'], ['b','alas'], ['c', 'cccc'], ['c', 'a']])
        # kategorie = np.array([1, 1, 1, 2, 2, 2])
        # print npFeaturesArray,npCategoriesArray
        # print self.clf, "fit"
        if klasyfikatorId in ('1','2','3','4'):
            self.clf.fit(npFeaturesArray, npCategoriesArray)
            joblib.dump(self.clf, self.baseFile)
        elif klasyfikatorId in ('5'):
            # print self.dictFeatures
            self.clf.add(Dense(len(npFeaturesArray[0]) / 120, input_dim=len(npFeaturesArray[0]), kernel_initializer='uniform',
                            activation='relu'))
            # model.add(Dense(self.lenFeatures/8, kernel_initializer='uniform', activation='sigmoid', use_bias=True))
            self.clf.add(Dense(len(self.categories), kernel_initializer='uniform', activation='sigmoid'))

            # Compile model
            # model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
            self.clf.compile(loss='binary_crossentropy',
                          optimizer='rmsprop', metrics=[f1NN])
            self.clf.fit(npFeaturesArray, npCategoriesArrayNN, epochs=20, batch_size=10, verbose=0, shuffle=True)
            # evaluate the model
            self.clf.save_weights(self.baseFile)
            # wyswietla podsumowanie modelu
            self.clf.summary()
        # print(clf.predict([['c', 'x']]))

    def __work(self,predictArray,klasyfikatorId):
        # print "elo", predictArray
        with open('resources/naiveBayesCategories.json', 'r') as outfile:
            wyniki = json.load(outfile)
        if klasyfikatorId in ('1', '2', '3', '4'):
            self.clf = joblib.load(self.baseFile)  # wyczytuje z pliku
            x = self.clf.predict(predictArray)
        elif klasyfikatorId in ('5'):
            self.clf = Sequential()
            self.clf.add(
                Dense(len(self.cechyArray) / 120, input_dim=len(self.cechyArray), kernel_initializer='uniform',activation='relu'))
            # model.add(Dense(self.lenFeatures / 3, kernel_initializer='uniform', activation='sigmoid', use_bias=True))
            self.clf.add(Dense(len(wyniki), kernel_initializer='uniform', activation='sigmoid'))

            self.clf.load_weights(self.baseFile)
            x = self.clf.predict(np.array(predictArray))
            if len(x)==1:
                x=[np.argmax(x)]
            else:
                z = []
                for y in x:
                    z.append(np.argmax(y))
                x = z
            # print x, len(x)
        else:
            print "BLADwork"

        # print self.clf, "work"

        # lub chyba dla NN redictions1 = model
        if self.subfolder in ('word2vec'):
            z = np.bincount(x).argmax()
            # print "wynik:", z, wyniki[str(z)]
            print "wynik:", wyniki[str(z)]
            return z, wyniki[str(z)]

        else:
            for out in x:
                # print "wynik:", out,wyniki[str(out)]
                print "wynik:", out,wyniki[str(out)]
                return  out,wyniki[str(out)]

    def workProcess(self, textsArray,klasyfikatorId):

            # self.lenFeatures = len(self.cechyArray)
        if self.subfolder in ('lematyzacja', 'slowosiec', "lematyzacja+slowosiec"):
            if self.cechyArray == {}:
                self.importFeatures()
            if self.rC == "int":
                XML = XMLTokenizationIntegerWords(self.categories, self.folder, self.subfolder, self.cechyArray, [], oneAcapit=self.oneAcapit, minAF=self.minAppearFeature)
                predictArray = XML.naiveBayesPrepereObject(textsArray)
            elif self.rC == "bool":
                XML = XMLTokenizationBooleanWords(self.folder, self.subfolder, self.cechyArray, [],
                                                  oneAcapit=self.oneAcapit, minAF=self.minAppearFeature)
                predictArray = XML.neuralNetworkPrepereObject(textsArray)
        elif self.subfolder in ('word2vec'):
            # w2v = word2vec.Word2Vec.load('2018/word2vec/w2v.word2vec')
            predictArray, labelsArray, labelsArrayNN = self.gensimClass.avgw2v('z',textsArray[0])
            self.cechyArray = [None] * self.gensimClass.size()
        return self.__work(predictArray,klasyfikatorId)

    def prepareBasesToFit(self):
        idArraySepareteArticle = self._separeteArticlesMultiCategory()
        if self.subfolder in ('lematyzacja', 'slowosiec', "lematyzacja+slowosiec"):
            self.__prepereFeatures()
            if self.rC == "int":
                XML = XMLTokenizationIntegerWords(self.categories, self.folder, self.subfolder, self.cechyArray, idArraySepareteArticle, oneAcapit=self.oneAcapit, minAF=self.minAppearFeature)
                XML.XMLBayesSave()
            elif self.rC == "bool":
                XML = XMLTokenizationBooleanWords(self.folder, self.subfolder, self.cechyArray, idArraySepareteArticle,
                                                  oneAcapit=self.oneAcapit, minAF=self.minAppearFeature)
                XML.neuralNetworkVectors(self.categories)
        elif self.subfolder in ('word2vec'):
            self.w2vmodel = self.gensimClass.preparew2v(idArraySepareteArticle)

    def fitProcess(self, klasyfikatorId=0, raportOnly=False):
        self._setClassifier(klasyfikatorId)
        if not raportOnly:
            if self.subfolder in ('lematyzacja', 'slowosiec', "lematyzacja+slowosiec"):
                if self.rC == "int":

                    XML = XMLTokenizationIntegerWords(self.categories, self.folder, self.subfolder, self.cechyArray, [], oneAcapit=self.oneAcapit, minAF=self.minAppearFeature)
                    npFeaturesArray, npCategoriesArrayNN, npCategoriesArray = XML.naiveBayesPrepereBase()
                elif self.rC == "bool":
                    XML = XMLTokenizationBooleanWords(self.folder, self.subfolder, self.cechyArray, [],
                                                      oneAcapit=self.oneAcapit, minAF=self.minAppearFeature)
                    npFeaturesArray, npCategoriesArrayNN, npCategoriesArray = XML.neuralNetworkPrepereBase(
                        self.categories)
            elif self.subfolder in ('word2vec'):
                arrCategoriesSave={}
                for i in range(len(self.categories)):
                    arrCategoriesSave[i] = self.categories[i]
                with open('resources/naiveBayesCategories.json', 'w+') as outfile:
                    json.dump(arrCategoriesSave, outfile)

                if self.w2vmodel is not None:
                    npFeaturesArray, npCategoriesArray, npCategoriesArrayNN = self.gensimClass.avgw2v(self.w2vmodel)
                    self.cechyArray = [None] * self.gensimClass.size()
                else:
                    print "Brak w2vmodel"
            self.__fit(npFeaturesArray, npCategoriesArrayNN, npCategoriesArray, klasyfikatorId)


    def _setClassifier(self,klasyfikatorId):
        """
        Zapisywany jest wybrany klasyfikator
        :param klasyfikatorId: #GaussianNB(0) KNeighborsClassifier(1) LogisticRegression(2) RandomForestClassifier(3)
        :return: Zapis do self.clf modelu
        """
        if klasyfikatorId == '1':
            self.clf = GaussianNB()
        elif klasyfikatorId == '2':
            self.clf = KNeighborsClassifier(n_neighbors=11, n_jobs=-1)
        elif klasyfikatorId == '3':
            self.clf = LogisticRegression(n_jobs=-1)
        elif klasyfikatorId == '4':
            self.clf = RandomForestClassifier(n_estimators=20, n_jobs=-1)
        elif klasyfikatorId == '5':
            self.clf = Sequential()


if __name__ == '__main__':
    import csv,copy as cp
    model = 'lematyzacja'
    # model = 'slowosiec'
    # model = "lematyzacja+slowosiec"
    # model = "word2vec"
    folder = '2018'

    klasyfikatorId = '3'  # GaussianNB(1) KNeighborsClassifier(2) LogisticRegression(3) RandomForestClassifier(4), sieci neuronowe (5)
    minAppearFeature = 3
    oneAcapit = True
    reperezentacjaCech = "int"
    # reperezentacjaCech = "bool"

    article = Article()
    lematyzacjaClass = LematizationClass(False)
    textsArray = []
    text = article.readText('resources/png2pdf.pdf')
    # text =u"białko i kot. Komórka z pomarańczowym mitochondrium."
    if model != 'word2vec':
        textsArray.append(lematyzacjaClass.lemmatization(lematyzacjaClass.removeChars(text)))
    else:
        XMLPreparation = XMLModelPreparation(folder, model, oneAcapit)
        textsArray.append(XMLPreparation.textLemmatizationGensim(text))
        print textsArray

    # text = article.readText('resources/test.docx')
    # textsArray.append(lematyzacjaClass.lemmatization(lematyzacjaClass.removeChars(text)))

    categories = ['biofizyka', 'curling', 'cukrownictwo']
    categories = [u'biologia komórki', u'biologia molekularna', u'biologia rozrodu']
    categories = [u'biologia komórki', u'biologia molekularna', u'biologia rozrodu', u'statystyka opisowa',
                  u'rośliny owocowe']
    categories = [u'biologia komórki', u'biologia molekularna']
    categories = [u'rośliny owocowe', u'biologia komórki', u'biologia molekularna']
    categories = [u'biologia komórki', u'biologia rozwoju', u'histologia', u'morfologia (biologia)', u'ewolucja']
    categories = [u'komórki', u'budowa ziemi', u'wiertnictwo', u'sole organiczne', u'naczynia']

    baseFile = 'piecKategoriiLRx.NN'
    filenameFeatures = 'features_NaiveBayes_2018-4-15_12-38-47.txt' # nie musi byc podawana, tylko w przypadku gdy uzywamy juz worku
    classNB = MachineLearningMain(categories, baseFile, filenameFeatures, False, folder, model,
                                                                                 onlySeparableCats=True, minAppearFeature=minAppearFeature,
                                                                                 oneAcapit=oneAcapit, rC=reperezentacjaCech)
    classNB.fitProcess('1')  # uczenie Bayes
    # with open(r'wynikiEX2.csv', 'a') as f:
    #     writer = csv.writer(f)
    #     writer.writerow(['model', 'minAppearFeature', 'oneAcapit', 'reperezentacjaCech', 'klasID', 'a', 'b'])
    # for model in ('lematyzacja', 'slowosiec', "lematyzacja+slowosiec"):
    #     if model=='slowosiec':
    #         oneAcapitSET = (False,)
    #     else:
    #         oneAcapitSET = (False, True)
    #     for minAppearFeature in (0, 3):
    #         for oneAcapit in oneAcapitSET:
    #             for reperezentacjaCech in ('bool', 'int'):
    #                 classNB = MachineLearningMain(categories, baseFile, filenameFeatures, False, folder, model,
    #                                               onlySeparableCats=True, minAppearFeature=minAppearFeature,
    #                                               oneAcapit=oneAcapit, rC=reperezentacjaCech)
    #                 classNB.prepareBasesToFit()
    #                 for klasID in ('1','2','3','4','5'):
    #                     classNB.fitProcess(klasyfikatorId) # uczenie Bayes
    #                     a,b = classNB.workProcess(cp.copy(textsArray),klasyfikatorId) # sprawdzanie Bayes
    #                     with open(r'wynikiEX2.csv', 'a') as f:
    #                         writer = csv.writer(f)
    #                         writer.writerow([model.encode('cp1250'),str(minAppearFeature),str(oneAcapit),reperezentacjaCech,klasID,str(a),b.encode('cp1250')])
    #                         print [model,minAppearFeature,oneAcapit,reperezentacjaCech,klasID,a,b]

    # with open(r'wynikiEX1.csv', 'a') as f:
    #     writer = csv.writer(f)
    #     writer.writerow(['model', 'minAppearFeature', 'oneAcapit', 'reperezentacjaCech', 'klasID', 'a', 'b'])
    # for model in ('word2vec',):
    #     oneAcapitSET = (False, True)
    #     for minAppearFeature in (0, 3):
    #         for oneAcapit in oneAcapitSET:
    #             for reperezentacjaCech in (200,5000):
    #                 for klasID in ('1', '2', '4', '5'):
    #                     classNB = MachineLearningMain(categories, baseFile, filenameFeatures, False, folder, model,
    #                                                   onlySeparableCats=True, minAppearFeature=minAppearFeature,
    #                                                   oneAcapit=oneAcapit, rC=reperezentacjaCech)
    #                     classNB.prepareBasesToFit()
    #                     classNB.fitProcess(klasyfikatorId)  # uczenie Bayes
    #                     a, b = classNB.workProcess(cp.copy(textsArray), klasyfikatorId)  # sprawdzanie Bayes
    #                     with open(r'wynikiEX1.csv', 'a') as f:
    #                         writer = csv.writer(f)
    #                         writer.writerow(
    #                             [model.encode('cp1250'), str(minAppearFeature), str(oneAcapit), reperezentacjaCech,
    #                              klasID, str(a), b.encode('cp1250')])
    #                         print [model, minAppearFeature, oneAcapit, reperezentacjaCech, klasID, a, b]
    # for model in ('word2vec',):
    #     oneAcapitSET = (False, True)
    #     for minAppearFeature in (0,3):
    #         for oneAcapit in oneAcapitSET:
    #             for reperezentacjaCech in (200,5000):
    #                 for klasID in ('1', '2', '4', '5'):
    #                     classNB = MachineLearningMain(categories, baseFile, filenameFeatures, False, folder, model,
    #                                                   onlySeparableCats=True, minAppearFeature=minAppearFeature,
    #                                                   oneAcapit=oneAcapit, rC=reperezentacjaCech)
    #                     classNB.prepareBasesToFit()
    #                     classNB.fitProcess(klasyfikatorId)  # uczenie Bayes
    #                     a, b = classNB.workProcess(cp.copy(textsArray), klasyfikatorId)  # sprawdzanie Bayes
    #                     with open(r'wynikiEX1.csv', 'a') as f:
    #                         writer = csv.writer(f)
    #                         writer.writerow(
    #                             [model.encode('cp1250'), str(minAppearFeature), str(oneAcapit), reperezentacjaCech,
    #                              klasID, str(a), b.encode('cp1250')])
    #                         print [model, minAppearFeature, oneAcapit, reperezentacjaCech, klasID, a, b]
