# -*- coding: utf8 -*-
import re,copy
from pyMorfologik import Morfologik
from pyMorfologik.parsing import ListParser, DictParser

class LematizationClass():

    def __init__(self, memory):
        """
        :param memory: bollean, True wczytywane sa wszystkei dane do pamieci Przydatne  gdy wykorzystujemy tylko jeden plik, a False dla gromady plikow czyli np dla nauczania
        """

        with open('resources/stopwordsWikipedia.txt', 'r') as outfile:
            self.stopwords = outfile.read().replace(" ", "").split(',')
        with open('resources/stopwordsMine.txt', 'r') as outfile:
            self.stopwords.extend(outfile.read().split(','))
        # print self.stopwords
        self.lower1char = lambda s: s[:1].lower() + s[1:] if s else ''
        self.upper1char = lambda s: s[:1].upper() + s[1:] if s else ''
        self.lematizationDict = {}
        self.memory = memory
        if memory:
            self.readDict()
        else:
            self.parser = DictParser()
            self.stemmer = Morfologik()

    def deleteNonRelevantData(self):
        with open('2018/polimorfologikMine.txt', 'w') as writeF:
            with open('2018/polimorfologik-2.1.txt', 'r') as readF:
                for line in readF:
                    writeF.write(line[:[m.start() for m in re.finditer(';', line)][1]] + '\n')

    def readDict(self):
        with open('2018/polimorfologikMine.txt', 'r') as readF:
            for line in readF:
                listMorf = line.split(";")
                try:
                    # if listMorf[0] in self.lematizationDict[listMorf[1][:-1]]: &aby sprawdzić czy nie ma duplikatów
                    #     print "asd"
                    self.lematizationDict[listMorf[1][:-1]].append(listMorf[0])

                except:
                    self.lematizationDict[listMorf[1][:-1]]=[listMorf[0]]

    def removeChars(self, text):
        # return re.sub(u'[^0-9a-zA-ZąćęłńóśźżƶĄĆĘŁŃÓŚŹŻƵ\n\t]+', ' ', text).encode('UTF8').split()
        return re.sub(u'[^a-zA-ZąćęłńóśźżƶĄĆĘŁŃÓŚŹŻƵ\n\t]+', ' ', text).encode('UTF8').split()

    def removeCharsUnicode(self, text):
        # return re.sub(u'[^0-9a-zA-ZąćęłńóśźżƶĄĆĘŁŃÓŚŹŻƵ\n\t]+', ' ', text).encode('UTF8').split()
        return re.sub(u'[^a-zA-ZąćęłńóśźżƶĄĆĘŁŃÓŚŹŻƵ\n\t]+', ' ', text).split()

    def createSenetncesArray(self, text):
        return text.replace('!', '.').replace('?', '.').replace("]]\n", '. ').replace('\n', ' ').split('. ')

    def lemmatization(self, textArray):
        """ potrzebne jest sprawdzanie z duzych i malych liter poniewaz moze byc to poczatek zdania a moze tez tobyc jakies wlasne slowo"""
        lemadict = {}
        notConverted = []
        lemaArray = []
        lemaArrayLast = []
        textArray = self.__validationLemmatizationArray(textArray)
        if self.memory:
            for word in textArray:
                try:
                    lemaArray.extend(self.lematizationDict[word])
                except:
                    try:
                        lemaArray.extend(self.lematizationDict[self.lower1char(word)])
                    except:
                        lemaArray.append(word)
            for word in lemaArray:
                # try:
                    if word not in self.stopwords:
                        lemaArrayLast.append(word.lower())
                # except:
                #     lemaArrayLast.append(word.lower())

        else:
            lemadictWithDuplicates = self.stemmer.stem(textArray, self.parser)
            for lema in lemadictWithDuplicates:
                lemadict[lema.encode('utf8')] = list(set(lemadictWithDuplicates[lema]))
            for text in textArray:
                if text not in lemadict:
                    notConverted.append(self.lower1char(text))

            lemadictWithDuplicatesw = self.stemmer.stem(notConverted,
                                                        self.parser)  # drugi raz dla wyrazow co zaczynaly sie z duzej litery.

            for lema in lemadictWithDuplicatesw:
                lemadict[self.upper1char(lema.encode('utf8'))] = list(set(lemadictWithDuplicatesw[lema]))
            for text in textArray:
                try:
                    lemaArray.extend(lemadict[text])
                except:
                    lemaArray.append(text)

            for word in lemaArray:
                try:
                    if word.encode('utf8') not in self.stopwords:
                        lemaArrayLast.append(word.lower())
                except:
                    lemaArrayLast.append(word.lower())



        return lemaArrayLast

    def __validationLemmatizationArray(self, lemaArrayLast):
        """ usuniecie niektorch slow"""
        # print lemaArrayLast
        lemaArrayOrg = copy.deepcopy(lemaArrayLast)
        for word in lemaArrayOrg:
            wordLen = len(word)
            if wordLen <= 2 or word.isdigit(): # !!!!!!!!!!!!! jezeli removeChars wyko,emtowane cyfry isdigit mozna skasowac
                lemaArrayLast.remove(word)
        # print lemaArrayLast
        return lemaArrayLast

if __name__ == '__main__':
    lematyzacjaClass = LematizationClass(True)
    # lematyzacja.deleteNonRelevantData()
    # try:
    #     print lematyzacjaClass.lematizationDict['kiciuś']
    # except:
    #     pass
    # print lematyzacjaClass.lematizationDict['kotek']

    text = u"Gotowanie makaronu Spaghetti składa się z długich(do ok. 80 cm), prostych i dość grubych nitek. Makaron produkuje się je z twardych odmian mąki pszenicznej tzw. pszenicy durum. "
    x = lematyzacjaClass.removeChars(text)
    print x
    y = lematyzacjaClass.lemmatization(x)
    print y
