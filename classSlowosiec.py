# -*- coding: utf-8 -*-

from classXMLManager import XMLManager
import igraph as ig

class synsetObject():

    def __init__(self, word):
        self.wordName = word
        self.idWordIN = 0
        self.idSynsetIN = 0
        self.synsetArrayOut = []  # pierwszy argument to id synsetu, drugi to typ
        self.wordsArrayOut = []  # pierwszy argument to id slowa, drugi to typ
        self.wordArrayOut = []

    def wyswietlWszystkieDane(self):
        print self.wordName
        print self.idWordIN
        print self.idSynsetIN
        print self.synsetArrayOut
        print self.wordsArrayOut
        print self.wordArrayOut

    def wyswietlPolaczenia(self):
        for i in range(len(self.wordArrayOut)):
            print self.wordName + " -> " + self.wordsArrayOut[i][1] + " -> " + self.wordArrayOut[i]

class slowosiecXML(XMLManager):

    def __init__(self, mode):
        """
        :param mode: XML wykorzystuje plik XML i wykonuje zapytania Xpath lub memory - wczytuje dane do pamieci
        """
        self.synsetObjectArray =[]
        self.synsetObjectPath=[]
        self.wordIDPath=[]
        self.mode = mode
        self.lenmaxDegree = 1000
        XMLManager.__init__(self)

    def readXML(self, filename):
        XMLManager.readXML(self, filename)
        if self.mode == "memory":
            self.lexicalUnitsById, self.lexicalUnitsByName = self.getLexicalUnitsMemory()
            self.synsetsById, self.synsetsByLexicalID = self.getSynsetsMemory()
            self.synsetrelations = self.getSynsetrelationsMemory()
            self.clearXMLvariables()

    def getLexicalUnitsMemory(self):
        lexicalUnitsById = {}
        lexicalUnitsByName = {}
        for lexicalUnit in self.root.xpath(u'/array-list/lexical-unit'):
            name = lexicalUnit.get('name')
            nameId = lexicalUnit.get('id')
            lexicalUnitsById[nameId] = name
            try:
                lexicalUnitsByName[name].append(lexicalUnit.get('id'))
            except:
                lexicalUnitsByName[name] = [nameId]
        return lexicalUnitsById, lexicalUnitsByName

    def getSynsetsMemory(self):
        synsetsById = {}
        synsetsByLexicalID = {}
        for synset in self.root.xpath(u'/array-list/synset'):
            synsetId = synset.get('id')
            unitIdArr = []
            for unitId in synset:
                lexicalId = unitId.text
                unitIdArr.append(lexicalId)
                synsetsByLexicalID[lexicalId] = synsetId # po analizach wordID ma tylko jeden synset
            synsetsById[synsetId]={}
            synsetsById[synsetId]['lexicalUnits'] = unitIdArr
        return synsetsById, synsetsByLexicalID

    def getSynsetrelationsMemory(self):
        for synsetrelation in self.root.xpath(u'/array-list/synsetrelations'):
            synsetIdParent = synsetrelation.get('parent')
            synsetIdChild = synsetrelation.get('child')
            synsetIdRelation = synsetrelation.get('relation')
            try:
                self.synsetsById[synsetIdParent][synsetIdRelation].append(synsetIdChild)
            except:
                try: #byc moze beda takei ID ktore zostaly wyrzucone
                    self.synsetsById[synsetIdParent][synsetIdRelation] = [synsetIdChild]
                except:
                    pass

    def deleteSlowosiecUnnecessaryRows(self):
        self.deleteAllWithoutPages(['lexicalrelations'])
        # self.deleteArgumentsInTags({'/array-list/lexical-unit':['pos','tagcount','domain','desc','workstate','source'],
        #                            '/array-list/synset':['workstate','split','owner','desc','abstract','definition'],
        #                             '/array-list/synsetrelations':['valid','owner']})
        # self.deletetagesWithArguments({'synsetrelations': {
        #     'relation': [[12,13],[16,19],[31,59],[66,111],[118,140],[143,144],[149,168],[170,244],[3000,3016],[3413,3464]]}})  # usuniecie relacjimiezyjezykowych oraz fuzzynima
            # 'relation': (10,11,14,15,20,21,22,23,24,25,26,27,28,29,30,60,61,62,63,64,65,112,113,114,115,116,117,141,142,145,148,169,3410,3412)}},[[10,244],[3000,3016],[3410,3464]])  # usuniecie relacjimiezyjezykowych oraz fuzzynima

        # self.deleteEnglishWords(262988, 380780)


    def deleteEnglishWords(self,fromIDSynset,toIDSynset):
        """ 
        Usuwa angielskie slowa i synsety
        :param fromIDSynset: id synset pierwszy
        :param toIDSynset:  id synset ostatni
        :return: null
        """
        for synset in self.root.xpath('/array-list/synset[@id>"' + str(fromIDSynset) + '" and @id<"' + str(toIDSynset) + '"]'):
            for idUnit in synset:
                for word in self.root.xpath('/array-list/lexical-unit[@id="' + str(idUnit.text) + '"]'):
                    print word.get('name')
                    word.getparent().remove(word)
            synset.getparent().remove(synset)


    def __getWordIDOfSynset(self,synsetId):
        """ Pobranie tablicy ID słów posiadajc ID synsetu"""
        wordIdArray=[]
        for idSynset in self.root.xpath('/array-list/synset[@id="'+str(synsetId)+'"]'):
            for idUnit in idSynset:
                wordIdArray.append(idUnit.text)
        return wordIdArray

    def __getWordFromID(self,wordId):
        return self.root.xpath('/array-list/lexical-unit[@id="'+str(wordId)+'"]')[0].get('name')

    def getIDOfWordXpath(self, word):
        """ krok1. pobranie id słowa"""
        for lexicalUnit in self.root.xpath(u'/array-list/lexical-unit[@name="'+word+u'"]'):
            synset = synsetObject(word)
            synset.idWordIN=lexicalUnit.get('id')
            self.synsetObjectArray.append(synset)

    def getIDOfSysnetXpath(self):
        """ krok2. pobranie id synsetu danego slowa"""
        for synsetObject in self.synsetObjectArray:
            for synset in self.root.xpath('/array-list/synset/unit-id[text()="'+str(synsetObject.idWordIN)+'"]'):
                synsetObject.idSynsetIN = synset.getparent().get('id')
        # print (self.idSynsetIN, self.idWordIN, 'getIDOfSysnet')

    def getIDsSynsetFromSynsetXpath(self, types):
        """ krok3. Znalezienie synset relations dla wejsciowego synseta pobranie wylacznie hiperonimow (11) i hiponimów (10) """
        for synsetObject in self.synsetObjectArray:
            for synsetrelations in self.root.xpath('/array-list/synsetrelations[@parent='+str(synsetObject.idSynsetIN)+']'):
                if synsetrelations.get('relation') in types:
                    synsetObject.synsetArrayOut.append([synsetrelations.get('child'),synsetrelations.get('relation')])
            if len(synsetObject.synsetArrayOut) == 0:
                print "brak powiązań!!"
        # print (types, self.idSynsetIN, self.synsetArrayOut, 'getIDsSynsetFromSynset')

    def getIDsOfWordOUTFromSynsetsXpath(self):
        """ krok4. Pobranie id słów po znalezionym synsecie"""
        for synsetObject in self.synsetObjectArray:
            for relationSynset in synsetObject.synsetArrayOut:
                wordIdArray = self.__getWordIDOfSynset(relationSynset[0])
                for wordId in wordIdArray:
                    synsetObject.wordsArrayOut.append([wordId,relationSynset[1]])

        # print (self.synsetArrayOut,wordIdArray, self.wordsArrayOut, 'getIDsOfWordOUTFromSynsets')

    def getWordsFromIdsXpath(self):
        """ krok5. Pobranie słów po znalezionych synsetach ID"""
        for synsetObject in self.synsetObjectArray:
            for wordArray in synsetObject.wordsArrayOut:
                synsetObject.wordArrayOut.append(self.__getWordFromID(wordArray[0]))
            # print (self.wordsArrayOut, returnArray, 'getWordsFromIds')

    def stepsFrom2to5(self, types):
        """
        :param types: (set) poszukiwane  relacje miedzy slwoami
        :return: extended self.synsetObjectArray
        """
        for synset in self.synsetObjectArray:
            if synset.idSynsetIN == 0 : # poniewaz funkcja getHiperonimsAndHiponimsFromSynset moze juz miec podany id synsetu
                synset.idSynsetIN = self.synsetsByLexicalID[synset.idWordIN]
            for type in self.synsetsById[synset.idSynsetIN]:
                if type in types:
                    for idSynsetOut in self.synsetsById[synset.idSynsetIN][type]:
                        synset.synsetArrayOut.append([idSynsetOut, type])
            for synsetOut in synset.synsetArrayOut:
                for wordOut in self.synsetsById[synsetOut[0]]['lexicalUnits']:
                    synset.wordsArrayOut.append([wordOut, synsetOut[1]])
            for wordsArray in synset.wordsArrayOut:
                synset.wordArrayOut.append(self.lexicalUnitsById[wordsArray[0]])

    def wyswietlSynsetObjectArray(self):
        for synsetObject in self.synsetObjectArray:
            synsetObject.wyswietlWszystkieDane()
            synsetObject.wyswietlPolaczenia()


    def getHiperonimsAndHiponimsFromSynset(self, word, wordID, idSynset, types):
        synset = synsetObject(word)
        synset.idWordIN = wordID
        if self.mode == "XML":
            if idSynset == 0:
                self.synsetObjectArray.append(synset)
                self.getIDOfSysnetXpath()
            else:
                synset.idSynsetIN = idSynset
                self.synsetObjectArray.append(synset)
            self.getIDsSynsetFromSynsetXpath(types)
            self.getIDsOfWordOUTFromSynsetsXpath()
            self.getWordsFromIdsXpath()
        if self.mode == "memory":
            try:
                if idSynset == 0:
                    synset.idSynsetIN = self.synsetsByLexicalID[synset.idWordIN]
                else:
                    synset.idSynsetIN = idSynset
                self.synsetObjectArray.append(synset)
                self.stepsFrom2to5(types)
            except:
                synset.idSynsetIN = -1

        # self.wyswietlSynsetObjectArray(),'x'
        return self.synsetObjectArray

    def getHiperonimsAndHiponimsFromWord(self,word,types):
        if self.mode == "XML":
            self.getIDOfWordXpath(word)
            self.getIDOfSysnetXpath()
            self.getIDsSynsetFromSynsetXpath(types)
            self.getIDsOfWordOUTFromSynsetsXpath()
            self.getWordsFromIdsXpath()
        elif self.mode == "memory":
            try:
                for wordId in self.lexicalUnitsByName[word]:
                    synset = synsetObject(word)
                    synset.idWordIN = wordId
                    self.synsetObjectArray.append(synset)
                self.stepsFrom2to5(types)
            except: #jezeli nie zostanie znalezione dane slowo
                synset = synsetObject(word)
                synset.idWordIN = -1
                synset.idSynsetIN = -1



        # self.wyswietlSynsetObjectArray(),'y'
        return self.synsetObjectArray

    def resetVariables(self):
        """ resetuje zmienne klasy"""
        self.synsetObjectArray = []


    def getPathTreeSlowosiec(self, word, types, setMaxDegree, mode=1, start=False, degree=0, wordID=0, idSynset=0):
        self.synsetObjectPath=[]
        self.wordIDPath=[]
        self.lenmaxDegree = setMaxDegree
        synsetObjectPath = self.treeSlowosiec(word, types, mode, start, degree, wordID, idSynset)
        self.resetVariables()
        return synsetObjectPath

    def drowSlowosiec(self, word, types, setMaxDegree=200, mode=1, start=False, degree=0, wordID=0, idSynset=0):
        self.synsetObjectPath = []
        self.wordIDPath = []
        self.lenmaxDegree = setMaxDegree
        synsetObjectPath = self.treeSlowosiec(word, types, mode, start, degree, wordID, idSynset)
        self.printTreeSlowosiec(mode)
        self.resetVariables()

    def treeSlowosiec(self, word, types, mode=1, start=False, degree=0, wordID=0, idSynset=0):
        """ Zbiera informacje dotyczace  
        mode - 0,1 : 0-niedokladny, 1- dokladny (roznica tkwi w rysowaniu dokladny-wolneijszy ale z synonimow sa tez strzalki (jedyne potrzbene w rysowaniu w lematyzacji nie bedzie potrzebne)"""
        if start:
            synsetObjectArray = self.getHiperonimsAndHiponimsFromWord(word, types)
        else:
            synsetObjectArray = self.getHiperonimsAndHiponimsFromSynset(word, wordID, idSynset, types)
        degree +=1
        self.synsetObjectPath.append(synsetObjectArray)
        self.resetVariables()
        for synsetObject in synsetObjectArray:
            if len(synsetObject.wordsArrayOut) > 0:
                self.wordIDPath.append(synsetObject.idWordIN) #dodanie do tablicy uzytych id slow
                if mode == 1:
                    for i in range(len(synsetObject.wordsArrayOut)): #zapewnia ze przejdziemy rpzez kazdy synset -> bierzemy ppeirwsza nazwe slwoa reszta to synonimy
                        # print synsetObject.wordArrayOut[i], types, False, synsetObject.wordsArrayOut[i][0]
                        if synsetObject.wordsArrayOut[i][0] not in self.wordIDPath and degree < self.lenmaxDegree:  # jezeli juz raz bylo znalezione to nie ma sensu sprawdzac raz jeszcze
                            self.treeSlowosiec(synsetObject.wordArrayOut[i], types, mode, False, degree, synsetObject.wordsArrayOut[i][0],0) # uwaga synsetObject.wordsArrayOut[i][0] przekazujemy tylko pierwszy nazwe, ale przy rysowaniu powinny byc wszystkie
                elif mode == 0:
                    for i in range(len(synsetObject.synsetArrayOut)): #zapewnia ze przejdziemy rpzez kazdy synset -> bierzemy ppeirwsza nazwe slwoa reszta to synonimy
                        # print synsetObject.wordArrayOut[i], types, False, synsetObject.synsetArrayOut[i][0],synsetObject.synsetArrayOut[i][0]
                        if synsetObject.wordsArrayOut[i][0] not in self.wordIDPath and degree < self.lenmaxDegree:  # jezeli juz raz bylo znalezione to nie ma sensu sprawdzac raz jeszcze
                            self.treeSlowosiec(synsetObject.wordArrayOut[i], types, mode, False, degree, synsetObject.wordsArrayOut[i][0],synsetObject.synsetArrayOut[i][0]) # uwaga synsetObject.wordsArrayOut[i][0] przekazujemy
        if start:
            return self.synsetObjectPath

    def printTreeSlowosiec(self, mode,synsetObjectPath=""):
        """ Rysuje drzewo hiperonimu lub hiponimow dla danego slowa """
        if synsetObjectPath != "":
            self.synsetObjectPath = synsetObjectPath
        pairs =[]
        # namesSynset =[]
        namesWords =[]
        namesWordID =[]
        colors =[]
        remebernamesIN=[]
        rememberSynsetIN=[]
        if self.synsetObjectPath ==[]:
            print "brak Danych"
        #tworzenie nazw i kolorow

        for obiectsStart in self.synsetObjectPath[0]:
            namesWords.append(obiectsStart.wordName.encode('utf8', 'replace'))  # pierwsza nazwa
            namesWordID.append(obiectsStart.idWordIN) #pierwsze ID
            colors.append('green')
        for synsetObjectArray in self.synsetObjectPath:
            for synsetObject in synsetObjectArray:
                # for synsetIN in synsetObject.idSynsetINArray:  # po wejsciach
                #     namesSynset.append(synsetIN)

                first=True
                for i in range(len(synsetObject.wordsArrayOut)):
                    if synsetObject.wordsArrayOut[i][0] not in namesWordID:
                        namesWordID.append(synsetObject.wordsArrayOut[i][0])
                        if first:
                            colors.append('red')
                            first = False
                        else:
                            colors.append('aqua')
                        namesWords.append(synsetObject.wordArrayOut[i].encode('utf8', 'replace'))
        #tworzenie par
        for synsetObjectArray in self.synsetObjectPath:
            for synsetObject in synsetObjectArray:

                    # przyporzadkowanie
                    for wordOut in synsetObject.wordsArrayOut:  #po wyjsciach
                        if wordOut[1] in ('11','14', '20', '21', '22', '23', '24', '64', '112', '113', '114', '3410'):
                            pairs.append((namesWordID.index(synsetObject.idWordIN), namesWordID.index(wordOut[0])))
                        elif wordOut[1] in ('10','15', '25', '26', '27', '28', '29', '65', '115', '116', '117', '3412'):
                            pairs.append((namesWordID.index(wordOut[0]), namesWordID.index(synsetObject.idWordIN)))
                        else:
                            pairs.append((namesWordID.index(synsetObject.idWordIN), namesWordID.index(wordOut[0])))
                            pairs.append((namesWordID.index(wordOut[0]), namesWordID.index(synsetObject.idWordIN)))

                    if mode ==0:
                        # #jezeli jakeis slowa z poprzedmiego mialo synonimy nalezy rownieaz je polaczyc (ew jednak mozna wywalic zeby od sunsetu szlo tylk odla kazdego word id wtedy rozwiazaloby problem ze s;owem np "Wytwor" oraz ponizszegokodu by bie byo, lecz strasilibysmy duuuzo na czasie
                        if len(set(synsetObject.idSynsetIN).intersection(set(rememberSynsetIN))):
                            for wordOut in synsetObject.wordsArrayOut:  # po wyjsciach
                                for wordIN in remebernamesIN:  #po wyjsciach
                                    if wordOut[1]=='11':
                                        pairs.append((namesWordID.index(wordIN[0]), namesWordID.index(wordOut[0])))
                                    elif wordOut[1]=='10':
                                        pairs.append((namesWordID.index(wordOut[0]), namesWordID.index(wordIN[0])))

                        #dodanie synonimow do kolenego fora
                        remebernamesIN = synsetObject.wordsArrayOut[1:]
                        rememberSynsetIN = []
                        for idsynset in synsetObject.synsetArrayOut:
                            rememberSynsetIN.append(idsynset[0])



        numberNodes = len(namesWordID)

        graph = ig.Graph(n=numberNodes, directed=True)
        graph.add_edges(pairs)
        visual_style = {}
        # visual_style["bbox"] = (3000, 2000)
        # visual_style["bbox"] = (2000, 1300)
        # visual_style["bbox"] = (1200, 1080)
        visual_style["bbox"] = (500, 420)
        visual_style["margin"] = 35
        visual_style["layout"] = graph.layout_sugiyama()
        graph.vs["label"] = namesWords
        graph.vs["color"] = colors

        ig.plot(graph, 'resources/sciezkaSlowosiec'+namesWords[0].decode('utf8', 'ignore').encode('ascii', 'ignore')+'.png', **visual_style)

    def mergesynsetObjectPathAndGetWords(self,slowosiecSynsetArray,slowosiecSynsetArray2):
        slowosiecSynsetArray.extend(slowosiecSynsetArray2)
        allWords = []
        for synsetArray in slowosiecSynsetArray:
            for synset in synsetArray:
                allWords.extend(synset.wordArrayOut)
        return list(set(allWords))

if __name__ == '__main__':
    import time
    start = time.time()
    folder = "2018"
    # slowoXML = slowosiecXML('XML')
    # slowoXML.readXML(folder + '/plwordnetMine.xml')

    """ Skasowanie niepotrzebnych informacji z XML"""
    # slowoXML.deleteSlowosiecUnnecessaryRows()
    # slowoXML.clearAndSafe(folder + '/plwordnetMine.xml')

    slowoXML2 = slowosiecXML('memory')
    slowoXML2.readXML(folder + '/plwordnetMine.xml')
    """ przyklad pobrania hiperonimu i hoponimu slowa kuchnia polska """
    # slowoXML2.getHiperonimsAndHiponimsFromWord("kuchnia polska",('10','11'))
    # slowoXML2.getHiperonimsAndHiponimsFromWord("cytologia",('10','11'))
    # slowoXML2.getHiperonimsAndHiponimsFromWord("kuchnia", ('10', '11'))

    """ przyklad wyswietlenia drzewa hiperonimu lub hiponimu slowa  """
    # slowoXML2.resetVariables()
    # slowoXML2.drowSlowosiec(u"kuchnia śląska",('10'),200,1,True)
    # slowoXML2.getPathTreeSlowosiec(u"kuchnia śląska",('10'),200,3,1,True)
    # slowosiecSynsetArray = slowoXML2.getPathTreeSlowosiec(u"kuchnia polska",('10'),200,3,1,True)
    # slowosiecSynsetArray2 = slowoXML2.getPathTreeSlowosiec(u"kuchnia polska",('11'),200,2,1,True)
    # allWords = slowoXML2.mergesynsetObjectPathAndGetWords(slowosiecSynsetArray,slowosiecSynsetArray2)
    # print allWords
    # slowoXML2.drowSlowosiec(u"wytwór", ('10'),200, 1 ,True)
    # slowoXML2.drowSlowosiec(u"kot",('10'),200,1,True)
    # slowoXML2.drowSlowosiec(u"pies",('10'),200,0,True)
    # slowoXML2.lenmaxDegree = 2
    # slowoXML2.drowSlowosiec(u"kuchnia śląska", ('10'),200, 1, True)
    # slowoXML2.drowSlowosiec(u"heterokarion", ('10'),2, 1, True)
    # x = slowoXML2.getPathTreeSlowosiec(u"heterokarion", ('10'), 2, 1, True)
    # print slowoXML2.mergesynsetObjectPathAndGetWords(x, [])
    # x = slowoXML2.getPathTreeSlowosiec(u"heterokarion", ('10'),2, 1, True)
    # print slowoXML2.mergesynsetObjectPathAndGetWords(x,[])
    # slowoXML2.drowSlowosiec(u"jedzenie", ('30','60','61','62','63','141','142','148','169'),200, 1, True)

    # slowoXML2.drowSlowosiec(u"system", ('10', '15', '25', '26', '27', '28', '29', '65', '115', '116', '117', '3412'),3, 1, True)'

    slowosiecSynsetArray = slowoXML2.getPathTreeSlowosiec(u"kuchnia polska", ('10'), 3, 1, True)
    slowosiecSynsetArray.extend(slowoXML2.getPathTreeSlowosiec(u"kuchnia polska", ('11'), 3, 1, True))
    slowosiecSynsetArray.extend(
        slowoXML2.getPathTreeSlowosiec(u"kuchnia polska", ('15', '25', '26', '27', '28', '29', '65', '115', '116', '117', '3412'), 10,
                                      1, True))
    slowosiecSynsetArray.extend(
        slowoXML2.getPathTreeSlowosiec(u"kuchnia polska", ('14', '20', '21', '22', '23', '24', '64', '112', '113', '114', '3410'), 10,
                                      1, True))
    slowosiecSynsetArray.extend(
        slowoXML2.getPathTreeSlowosiec(u"kuchnia polska", ('30', '60', '61', '62', '63', '141', '142', '148', '169'), 200, 1, True))

    slowoXML2.printTreeSlowosiec(1,slowosiecSynsetArray)

    start2 = time.time()
    print start2 - start
