# -*- coding: utf-8 -*-

from lxml import etree, objectify


class XMLManager:

    def __init__(self, folder=''):
        """ Inicjalizacja zmiennych """
        self.filePath, self.tree, self.root = '', '', None
        if folder != '':
            self.folder = folder+'/'
        else:
            self.folder = ''

    def readXML(self, filepath):
        """ Odczyt z pliku oraz zmienne"""
        print u"\t Wczytywanie pliku " +self.folder+filepath
        del self.filePath, self.tree, self.root
        self.filePath = self.folder+filepath
        self.tree = etree.parse(self.filePath)
        self.root = self.tree.getroot()
        objectify.deannotate(self.root, cleanup_namespaces=True)

    def clearAndSafe(self, newFile=''):
        """ Zapis do pliku """
        if newFile != '':
            self.filePath = self.folder+newFile
        print u"\t Zapis " + self.filePath
        objectify.deannotate(self.root, cleanup_namespaces=True)
        tree = etree.ElementTree(self.root)
        try:
            tree.write(self.filePath, xml_declaration=True, pretty_print=True, encoding="utf-8")
        except:
            pass

    def deleteAllWithoutPages(self, array2Delete):
        """ Usunięcie wszystkich niepotrzebnych tagów w strukturze XML"""
        print u"\t Usunięcie tagów"
        for tag in array2Delete:
            for element in self.root.xpath('//'+tag):
                element.getparent().remove(element)

    def clearXMLvariables(self):
        self.filePath, self.tree, self.root = '', '', None