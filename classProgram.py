# -*- coding: utf-8 -*-
import warnings
warnings.filterwarnings("ignore")
import igraph.vendor.texttable
import copy as cp
from classArticle import Article
from classLematization import LematizationClass
from classXMLModelPreparation import XMLModelPreparation
from classSlowosiec import slowosiecXML
from XMLFirst import XMLWikipedia
from Tkinter import *
import tkFileDialog


class InterfaceClass():

    def __init__(self):
        self.tasks = ('1','2','3','4','5','6','7') # 1-

        self.folderBase = '2018'

    def validateInputs(self, input):
        if input[0] in self.tasks:
            if input[0] == '1': # zwiazane z baza
                with open(input[2]) as file:
                    config = file.readlines()
                config = [x.strip() for x in config]

                if input[1] == '1': #fit
                    self.__machinelearningFit(config,input)
                elif input[1] == '2': #work
                    self.__machinelearningWork(config,input)
                else:
                    self.__printError(u"Nie poprawne zadanie - drugi argument")
                    return

            elif input[0]=='2': #rysowanie diagramów
                if input[2] in ('sug', 'rei'):
                    self.__drowGraphWiki(input)
                else:
                    self.__printError("bledne argument trzeci ('sug', 'rei')")
                    return

            elif input[0]=='3': #wypisz kategorie
                XML = XMLWikipedia(self.folderBase)
                XML.loadCategoriesTable()
                XML.readXMLCats('plwikiCats.xml')
                XML.buildCategoryMapCats()
                XML.readXML('plwikiArticlesX.xml')
                if isinstance(input[1],unicode):
                    XML.returnCatsFromArticlePage(input[1])
                else:
                    XML.returnCatsFromArticlePage(unicode(input[1].decode('cp1250')))


            elif input[0]=='4':  #wypisz tekst artykulu
                XML = XMLWikipedia(self.folderBase)
                XML.loadCategoriesTable()
                XML.readXML('plwikiArticlesX.xml')
                if isinstance(input[1], unicode):
                    XML.findArticle(input[1])
                else:
                    XML.findArticle(unicode(input[1].decode('cp1250')))

            elif input[0] == '5': #wygeneruje raporty csv
                if input[1] == '1':
                    XML = XMLWikipedia(self.folderBase)
                    XML.loadCategoriesTable()

                    XML.readXML('plwikiArticlesX.xml')
                    XML.export(['categoryCSV'])
                elif input[1] == '2':
                    modeCechy="lematyzacja"
                    categories = input[2:]
                    XMLWiki = XMLWikipedia(self.folderBase)
                    XMLWiki.loadCategoriesTable()
                    for category in categories:  # trzeba wykonowyac przed kazdym nauczaniem (z powodu mozliwosci artykulow wystepujacych w kilku kategoriach, a robimy tylko rozlaczne)
                        XMLWiki.readXML(modeCechy + '/' + category + '.xml')
                        XMLWiki.export(['wordsNumberCSV'], cp.copy(category), onlySeparableCats=True, categoriesArr=cp.copy(categories))


            elif input[0] == '6':
                self.__slowosiecDrowing(input)

            elif input[0] == '7':
                with open(input[1]) as file:
                    config = file.readlines()
                config = [x.strip() for x in config]
                kodowanie = config[0].decode('cp1250')
                akapit = config[1].decode('cp1250')
                minAppearFeature = int(config[2].decode('cp1250'))
                rC = config[3].decode('cp1250')
                klasyfikator = config[4].decode('cp1250')

                model = self.__returnModelFromText(kodowanie)
                akapit = self.__returmAkapitFromText(akapit)
                klasyfikatorId = self.__returmKlasyfikatorFromText(klasyfikator)

                filenameFeatures = ''

                categories = input[2:]
                for i in range(len(categories)):
                    if isinstance(categories[i], unicode):
                        pass
                    else:
                        categories[i] = unicode(categories[i].decode('cp1250'))

                print categories

                from classMachineLearningMain import MachineLearningMain
                classNB = MachineLearningMain(categories, '', filenameFeatures, False, self.folderBase, model,
                                              onlySeparableCats=True, minAppearFeature=minAppearFeature,
                                              oneAcapit=akapit, rC=rC)
                classNB.prepareBasesToFit()

        else:
            self.__printError(u"Niepoprawne zadanie - pierwszy argument")
            return

    def __printError(self, textError):
        print textError

    def __slowosiecDrowing(self, input):
        hiperonimyMaxDegree = int(input[1])
        hiponimyMaxDegree = int(input[2])
        meronimyMaxDegree = int(input[3])
        holonimyMaxDegree = int(input[4])
        synonimyBool = int(input[5])
        if isinstance(input[6], unicode):
            artykul = input[6].lower()
        else:
            artykul = unicode(input[6].decode('cp1250').lower())



        slowoXML2 = slowosiecXML('memory')
        slowoXML2.readXML('2018/plwordnetMine.xml')
        slowosiecSynsetArray = []
        if hiperonimyMaxDegree > 0:
            slowosiecSynsetArray.extend(slowoXML2.getPathTreeSlowosiec(artykul, ('10'), hiperonimyMaxDegree, 1, True))
        if hiponimyMaxDegree > 0:
            slowosiecSynsetArray.extend(slowoXML2.getPathTreeSlowosiec(artykul, ('11'), hiponimyMaxDegree, 1, True))
        if meronimyMaxDegree > 0:
            slowosiecSynsetArray.extend(
                slowoXML2.getPathTreeSlowosiec(artykul, (
                    '15', '25', '26', '27', '28', '29', '65', '115', '116', '117', '3412'), meronimyMaxDegree,
                                               1, True))
        if holonimyMaxDegree > 0:
            slowosiecSynsetArray.extend(
                slowoXML2.getPathTreeSlowosiec(artykul, (
                    '14', '20', '21', '22', '23', '24', '64', '112', '113', '114', '3410'), holonimyMaxDegree,
                                               1, True))
        if synonimyBool > '0':
            slowosiecSynsetArray.extend(
                slowoXML2.getPathTreeSlowosiec(artykul,
                                               ('30', '60', '61', '62', '63', '141', '142', '148', '169'), 200, 1,
                                               True))

        slowoXML2.printTreeSlowosiec(1, slowosiecSynsetArray)

    def __drowGraphWiki(self,input):
        XML = XMLWikipedia(self.folderBase)
        XML.loadCategoriesTable()  # zawsze musi byc wczytane
        XML.readXMLCats('plwikiCats.xml')
        XML.buildCategoryMapCats()
        XML.readXML('plwikiArticlesX.xml')
        if input[1] == '1':  # artykuly
            if isinstance(input[3], unicode):
                XML.drowCatsPath(input[3], 'article', input[2])
            else:
                XML.drowCatsPath(unicode(input[3].decode('cp1250')), 'article', input[2])

        elif input[1] == '2':  # kategorie
            if isinstance(input[3], unicode):
                XML.drowCatsPath(input[3], 'category', input[2])
            else:
                XML.drowCatsPath(unicode(input[3].decode('cp1250')), 'category', input[2])
        else:
            self.__printError(u"Niepoprawny drugi argument")
            return

    def __returnNodelFromInt(self, intModel):
        if intModel == u'1':
            model = 'lematyzacja'
        elif intModel == u'2':
            model = 'slowosiec'
        elif intModel == u'3':
            model = 'lematyzacja+slowosiec'
        elif intModel == u'4':
            model = 'word2vec'
        else:
            self.__printError(u"Nie poprawny klasyfikator lub rodzaj wejsc (er2)")
            return
        return model

    def __returnModelFromText(self, textModel):
        if textModel == u'wektor słów':
            model = 'lematyzacja'
        elif textModel == u'powiązania leksykalne':
            model = 'slowosiec'
        elif textModel == u'wektor słów + powiązania leksykalne':
            model = 'lematyzacja+slowosiec'
        elif textModel == u'word2vec':
            model = 'word2vec'
        elif textModel == u'doc2vec':
            model = 'doc2vec'
        else:
            self.__printError(u"Nie poprawne kodowanie")
            return
        return model

    def __returmAkapitFromText(self,akapitConfig):
        if akapitConfig == u'cały':
            akapit = False
        elif akapitConfig == u'paragraf':
            akapit = True
        else:
            self.__printError(u"Nie poprawny atrybut tekstu")
            return
        return akapit


    def __returmKlasyfikatorFromText(self, klasyfikatorConfig):
        if klasyfikatorConfig == 'NB':
            model = '1'
        elif klasyfikatorConfig == 'RF':
            model = '4'
        elif klasyfikatorConfig == 'KN':
            model = '2'
        elif klasyfikatorConfig == 'LR':
            model = '3'
        elif klasyfikatorConfig == 'NN':
            model = '5'
        else:
            self.__printError(u"Nie poprawne kodowanie")
            return
        return model



    def __machinelearningFit(self, config, input):
        print config
        kodowanie = config[0].decode('cp1250')
        akapit = config[1].decode('cp1250')
        minAppearFeature = int(config[2].decode('cp1250'))
        rC = config[3].decode('cp1250')
        klasyfikator = config[4].decode('cp1250')

        model = self.__returnModelFromText(kodowanie)
        akapit = self.__returmAkapitFromText(akapit)
        klasyfikatorId = self.__returmKlasyfikatorFromText(klasyfikator)


        baseFile = input[3]
        filenameFeatures = ''

        categories = input[4:]
        for i in range(len(categories)):
            if isinstance(categories[i], unicode):
                pass
            else:
                categories[i] = unicode(categories[i].decode('cp1250'))


        print categories

        from classMachineLearningMain import MachineLearningMain
        classNB = MachineLearningMain(categories, baseFile, filenameFeatures, False, self.folderBase, model, onlySeparableCats=True, minAppearFeature=minAppearFeature, oneAcapit=akapit, rC=rC)
        classNB.prepareBasesToFit()
        classNB.fitProcess(klasyfikatorId)



    def __machinelearningWork(self, config, input):
        kodowanie = config[0].decode('cp1250')
        akapit = config[1].decode('cp1250')
        minAppearFeature = int(config[2].decode('cp1250'))
        rC = config[3].decode('cp1250')
        klasyfikator = config[4].decode('cp1250')

        model = self.__returnModelFromText(kodowanie)
        akapit = self.__returmAkapitFromText(akapit)
        klasyfikatorId = self.__returmKlasyfikatorFromText(klasyfikator)

        categories = []
        baseFile = input[3]
        filenameFeatures = input[4]

        filesArray = input[5:]
        article = Article()
        lematyzacjaClass = LematizationClass(False)
        textsArray = []
        print u"\nPrzygotowanie artykułu"
        for filePath in filesArray:
            text = article.readText(filePath)
            if model != 'word2vec':
                textsArray.append(lematyzacjaClass.lemmatization(lematyzacjaClass.removeChars(text)))
            else:
                XMLPreparation = XMLModelPreparation(self.folderBase, model, akapit)
                textsArray.append(XMLPreparation.textLemmatizationGensim(text))
        from classMachineLearningMain import MachineLearningMain
        print u"\nUczenie maszynowe"
        classNB = MachineLearningMain(categories, baseFile, filenameFeatures, False, self.folderBase, model, onlySeparableCats=True, minAppearFeature=minAppearFeature, oneAcapit=akapit, rC=rC)
        classNB.workProcess(textsArray,klasyfikatorId)  # sprawdzanie Bayes










def kasuj(event):
    mGui.destroy()

def kasuj1():
    mGui.destroy()

def zaladujplik():
    global config
    mGui.fileName=tkFileDialog.askopenfilename(filetypes=(("Files", ".txt"), ("All Files", "*.*")))
    config = mGui.fileName

def zaladujplik1():
    global config1
    mGui.fileName=tkFileDialog.askopenfilename(filetypes=(("All Files", "*.*"),))
    config1 = mGui.fileName

def zaladujplik2():
    global config2
    mGui.fileName=tkFileDialog.askopenfilename(filetypes=(("Files", ".txt"), ("All Files", "*.*")))
    config2 = mGui.fileName

def zaladujplik3():
    global config3
    mGui.fileName=tkFileDialog.askopenfilename(filetypes=(("All Files", "*.*"),))
    config3 = mGui.fileName

def modeSelection():
    global tryb_programu
    tryb_programu =mode.get()

def mode2Selection():
    global tryb_programu2
    tryb_programu2 =mode2.get()

def modeDrowSelection():
    global tryb_drow
    tryb_drow =modeDrow.get()

def przypiszArtykul():
    mGui.mainloop()
    artykul=artykulTK.get()
    if artykul=="":
        artykul="brak"
    return artykul

def przypiszKategorie():
    mGui.mainloop()
    artykul=kategorieTK.get()
    if artykul=="":
        artykul="brak"
    return artykul


def przypiszSlowo():
    mGui.mainloop()
    artykul = slowoTK.get()
    if artykul == "":
        artykul = "brak"
    return artykul

def przypiszHiperonimy():
    mGui.mainloop()
    artykul = hiperTk.get()
    if artykul == "":
        artykul = "brak"
    return int(artykul)

def przypiszHiponimy():
    mGui.mainloop()
    artykul = hipoTk.get()
    if artykul == "":
        artykul = "brak"
    return int(artykul)

def przypiszMeronimy():
    mGui.mainloop()
    artykul = meroTk.get()
    if artykul == "":
        artykul = "brak"
    return int(artykul)

def przypiszHolonimy():
    mGui.mainloop()
    artykul = holoTk.get()
    if artykul == "":
        artykul = "brak"
    return int(artykul)

def przypiszSynonimy():
    mGui.mainloop()
    artykul = synTk.get()
    if artykul == "":
        artykul = "brak"
    return int(artykul)

if __name__ == '__main__':
    interface = InterfaceClass()
    input = sys.argv[1:]
    # print input
    if input != []:
        # input = ['1','1','resources/config.txt','plik','komórki']
        # interface.validateInputs(input)
        pass
    else:
        mGui = Tk()
        screen_width = mGui.winfo_screenwidth()
        screen_height = mGui.winfo_screenheight()
        szerokosc = int(0.35 * screen_width)
        wysokosc = int(0.2 * screen_height)
        mGui.geometry('280x230+' + str(szerokosc) + '+' + str(wysokosc))
        mGui.title('Kategoryzacja')
        mlabel1 = Label(text='Wybierz tryb', fg='red', bg='gray88').pack()
        mode = IntVar()
        Radio_1 = Radiobutton(mGui, text='Uczenie maszynowe', value=1, variable=mode, command=modeSelection).pack()
        Radio_2 = Radiobutton(mGui, text='Rysowanie drzewa kategorii', value=2, variable=mode, command=modeSelection).pack()
        Radio_2 = Radiobutton(mGui, text='Rysowanie drzewa połączeń leksykalnych', value=6, variable=mode, command=modeSelection).pack()
        Radio_3 = Radiobutton(mGui, text='Kategorie artykułu', value=3, variable=mode, command=modeSelection).pack()
        Radio_4 = Radiobutton(mGui, text='Szukaj artykułu', value=4, variable=mode, command=modeSelection).pack()
        Radio_5 = Radiobutton(mGui, text='Raporty', value=5, variable=mode, command=modeSelection).pack()
        mlabel2 = Label(text='').pack()
        mGui.bind("<Return>", kasuj)
        mbutton = Button(text='OK', command=kasuj1, fg='red', bg='white').pack()

        mGui.mainloop()
        # try:
        #     print tryb_programu
        # except:
        #     print "nie wybrano trybu"

        del mGui
        mGui = Tk()
        mGui.title('Kategoryzacja')
        input.append(str(tryb_programu))
        if tryb_programu == 1:
            mode2 = IntVar()
            modeDrow = StringVar()
            mGui.geometry('280x130+' + str(szerokosc) + '+' + str(wysokosc))
            mlabel1 = Label(text='Wybierz opcje', fg='red', bg='gray88').pack()
            Radio_1 = Radiobutton(mGui, text='Fit', value=1, variable=mode2,
                                  command=mode2Selection).pack()
            Radio_2 = Radiobutton(mGui, text='Work', value=2, variable=mode2,
                                  command=mode2Selection).pack()
            mlabel2 = Label(text='').pack()
            mGui.bind("<Return>", kasuj)
            mbutton = Button(text='OK', command=kasuj1, fg='red', bg='white').pack()
            mGui.mainloop()
            input.extend(str(tryb_programu2))


            if tryb_programu2 == 1:
                del mGui
                mGui = Tk()
                kategorieTK = StringVar()
                artykulTK = StringVar()
                mGui.title('Kategoryzacja')
                mGui.geometry('280x230+' + str(szerokosc) + '+' + str(wysokosc))
                Label(text='').pack()
                mbutton = Button(text='Plik Konfiguracyjny', command=zaladujplik, width=25, fg='red', bg='white').pack()
                Label(text='').pack()
                Label(text='Nazwa pliku modelu', fg='red', bg='gray88').pack()
                Entry(textvariable=artykulTK).pack()
                Label(text='np. model.pkl').pack()
                artykulTK.set("")

                Label(text='Podaj kategorie (separator ,)', fg='red', bg='gray88').pack()
                Entry(textvariable=kategorieTK).pack()
                Label(text='').pack()
                kategorieTK.set("")

                mGui.bind("<Return>", kasuj)
                mbutton = Button(text='OK', command=kasuj1, fg='red', bg='white').pack()

                kategorie = przypiszKategorie()
                plik = przypiszArtykul()
                mGui.mainloop()
                input.extend([config,plik])
                input.extend(kategorie.split(','))

            if tryb_programu2 == 2:
                del mGui
                mGui = Tk()
                mGui.title('Kategoryzacja')
                mGui.geometry('280x180+' + str(szerokosc) + '+' + str(wysokosc))
                Label(text='').pack()
                mbutton = Button(text='Plik Konfiguracyjny', command=zaladujplik, width=25, fg='red', bg='white').pack()
                mbutton = Button(text='Plik z modelem', command=zaladujplik1, width=25, fg='red', bg='white').pack()
                mbutton = Button(text='Plik z cechami', command=zaladujplik2, width=25, fg='red', bg='white').pack()
                mbutton = Button(text='Plik testowy', command=zaladujplik3, width=25, fg='red', bg='white').pack()
                Label(text='').pack()
                mGui.bind("<Return>", kasuj)
                mbutton = Button(text='OK', command=kasuj1, fg='red', bg='white').pack()

                mGui.mainloop()
                input.extend([config, config1, config2, config3])


        elif tryb_programu == 2:
            mode2 = IntVar()
            modeDrow = StringVar()
            mGui.geometry('280x230+' + str(szerokosc) + '+' + str(wysokosc))
            mlabel1 = Label(text='Wybierz opcje', fg='red', bg='gray88').pack()
            Radio_1 = Radiobutton(mGui, text='Artykuł', value=1, variable=mode2,
                                  command=mode2Selection).pack()
            Radio_2 = Radiobutton(mGui, text='Kategoria', value=2, variable=mode2,
                                  command=mode2Selection).pack()
            mlabel2 = Label(text='').pack()

            mlabel1 = Label(text='Wybierz layout', fg='red', bg='gray88').pack()
            Radio_3 = Radiobutton(mGui, text='układ Reingold Tilforda', value='rei', variable=modeDrow,
                                  command=modeDrowSelection).pack()
            Radio_4 = Radiobutton(mGui, text='uklad Sugiyama', value='sug', variable=modeDrow,
                                  command=modeDrowSelection).pack()
            mlabel2 = Label(text='').pack()

            mGui.bind("<Return>", kasuj)
            mbutton = Button(text='OK', command=kasuj1, fg='red', bg='white').pack()
            mGui.mainloop()
            input.extend([str(tryb_programu2),tryb_drow])

            if tryb_programu2 ==1:
                stringdod = "artykułu"
            else:
                stringdod = "kategorii"

            del mGui
            mGui = Tk()
            kategorieTK = StringVar()
            mGui.title('Kategoryzacja')
            mGui.geometry('280x280+' + str(szerokosc) + '+' + str(wysokosc))
            Label(text='Podaj tytuł '+stringdod, fg='red', bg='gray88').pack()
            Entry(textvariable=kategorieTK).pack()
            Label(text='').pack()
            kategorieTK.set("")

            mGui.bind("<Return>", kasuj)
            mbutton = Button(text='OK', command=kasuj1, fg='red', bg='white').pack()
            kategorie = przypiszKategorie()
            mGui.mainloop()
            input.append(kategorie)

        elif tryb_programu == 3:
            artykulTK = StringVar()
            mGui.geometry('280x100+' + str(szerokosc) + '+' + str(wysokosc))
            Label(text='Podaj nazwę artykułu', fg='red', bg='gray88').pack()
            Entry(textvariable=artykulTK).pack()
            Label(text='').pack()
            artykulTK.set("")

            mGui.bind("<Return>", kasuj)
            mbutton = Button(text='OK', command=kasuj1, fg='red', bg='white').pack()
            artykul = przypiszArtykul()
            mGui.mainloop()
            input.append(artykul)
            interface.validateInputs(input)

        elif tryb_programu == 4:
            artykulTK = StringVar()
            mGui.geometry('280x100+' + str(szerokosc) + '+' + str(wysokosc))
            Label(text='Podaj nazwę artykułu', fg='red', bg='gray88').pack()
            Entry(textvariable=artykulTK).pack()
            Label(text='').pack()
            artykulTK.set("")

            mGui.bind("<Return>", kasuj)
            mbutton = Button(text='OK', command=kasuj1, fg='red', bg='white').pack()
            artykul = przypiszArtykul()
            mGui.mainloop()
            input.append(artykul)

        elif tryb_programu == 5:
            mode2 = IntVar()
            mGui.geometry('280x150+' + str(szerokosc) + '+' + str(wysokosc))
            mlabel1 = Label(text='Wybierz opcje', fg='red', bg='gray88').pack()
            Radio_1 = Radiobutton(mGui, text='Eksport kategorii', value=1, variable=mode2, command=mode2Selection).pack()
            Radio_2 = Radiobutton(mGui, text='Eksport słów kategorii', value=2, variable=mode2,
                                  command=mode2Selection).pack()
            Radio_3 = Radiobutton(mGui, text='Raport wystąpień słów w zbiorze kategorii', value=3, variable=mode2,
                                  command=mode2Selection).pack()
            mlabel2 = Label(text='').pack()
            mGui.bind("<Return>", kasuj)
            mbutton = Button(text='OK', command=kasuj1, fg='red', bg='white').pack()

            mGui.mainloop()

            # try:
            #     print tryb_programu2
            # except:
            #     print "nie wybrano trybu"
            input.append(str(tryb_programu2))
            if tryb_programu2 == 1:
                pass
            elif tryb_programu2 == 2:
                del mGui
                mGui = Tk()
                kategorieTK = StringVar()
                mGui.title('Kategoryzacja')
                mGui.geometry('280x100+' + str(szerokosc) + '+' + str(wysokosc))
                Label(text='Podaj kategorie (separator ,)', fg='red', bg='gray88').pack()
                Entry(textvariable=kategorieTK).pack()
                Label(text='').pack()
                kategorieTK.set("")

                mGui.bind("<Return>", kasuj)
                mbutton = Button(text='OK', command=kasuj1, fg='red', bg='white').pack()
                kategorie = przypiszKategorie()
                mGui.mainloop()
                input.extend(kategorie.split(','))
            elif tryb_programu2 == 3:
                del mGui
                mGui = Tk()
                kategorieTK = StringVar()
                mGui.title('Kategoryzacja')
                mGui.geometry('280x170+' + str(szerokosc) + '+' + str(wysokosc))
                Label(text='').pack()
                mbutton = Button(text='Plik Konfiguracyjny', command=zaladujplik, width=25, fg='red', bg='white').pack()
                Label(text='').pack()
                Label(text='Podaj kategorie (separator ,)', fg='red', bg='gray88').pack()
                Entry(textvariable=kategorieTK).pack()
                Label(text='').pack()
                kategorieTK.set("")

                mGui.bind("<Return>", kasuj)
                mbutton = Button(text='OK', command=kasuj1, fg='red', bg='white').pack()
                kategorie = przypiszKategorie()
                mGui.mainloop()

                input = ['7',config]
                input.extend(kategorie.split(','))

        elif tryb_programu == 6:
            slowoTK = StringVar()
            hiperTk = IntVar()
            hipoTk = IntVar()
            meroTk = IntVar()
            holoTk = IntVar()
            synTk = IntVar()

            mGui.geometry('280x400+' + str(szerokosc) + '+' + str(wysokosc))
            Label(text='Podaj słowo', fg='red', bg='gray88').pack()
            Entry(textvariable=slowoTK).pack()
            Label(text='').pack()
            slowoTK.set("")

            Label(text='Podaj max. stopień hiperonimów', fg='red', bg='gray88').pack()
            Entry(textvariable=hiperTk).pack()
            Label(text='').pack()
            hiperTk.set("2")

            Label(text='Podaj max. stopień hiponimów', fg='red', bg='gray88').pack()
            Entry(textvariable=hipoTk).pack()
            Label(text='').pack()
            hipoTk.set("1")

            Label(text='Podaj max. stopień meronimów', fg='red', bg='gray88').pack()
            Entry(textvariable=meroTk).pack()
            Label(text='').pack()
            meroTk.set("10")

            Label(text='Podaj max. stopień holonimów', fg='red', bg='gray88').pack()
            Entry(textvariable=holoTk).pack()
            Label(text='').pack()
            holoTk.set("10")

            Label(text='Wyświetlać synonimy? 0-nie; 1-tak', fg='red', bg='gray88').pack()
            Entry(textvariable=synTk).pack()
            Label(text='').pack()
            synTk.set("1")

            mGui.bind("<Return>", kasuj)
            mbutton = Button(text='OK', command=kasuj1, fg='red', bg='white').pack()

            slowo = przypiszSlowo()
            hiperonimy = przypiszHiperonimy()
            hiponimy = przypiszHiponimy()
            meronimy = przypiszMeronimy()
            holonimy = przypiszHolonimy()
            synonimy = przypiszSynonimy()

            mGui.mainloop()
            input.extend([hiperonimy,hiponimy,meronimy,holonimy,synonimy,slowo])
            # print input


    interface.validateInputs(input)
    print u"Zakończono"
    raw_input()