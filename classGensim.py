# -*- coding: utf-8 -*-
from gensim.models import word2vec
from gensim.models import doc2vec
from classArticle import Article
from classLematization import LematizationClass
from XMLFirst import XMLWikipedia
import numpy as np

import json

class LabeledLineSentence(object):
    def __init__(self, doc_list, labels_list):
       self.labels_list = labels_list
       self.doc_list = doc_list
    def __iter__(self):
        for idx, doc in enumerate(self.doc_list):
            yield doc2vec.TaggedDocument(words=doc.split(),tags=[self.labels_list[idx]])


class GensimClass:
    def __init__(self, folder, mode, categories, oneAcapit, minAppearFeature, size=5000):
        self.mode = mode
        self.folder = folder
        self.categories = categories
        self.textsArray = []
        self.labelsArray = []
        self.catsDicts = {}
        self.num_features = size
        self.min_word_count = minAppearFeature
        self.num_workers = 8
        self.context = 25
        self.downsampling = 1e-3
        if oneAcapit:
            self.oneAcapitFilename = "_oneAcapit"
        else:
            self.oneAcapitFilename = ""

    def size(self):
        return self.num_features

    def preparew2v(self,idArraySepareteArticle):
        catsIndex = 0
        XMLWiki = XMLWikipedia(self.folder)
        for category in self.categories:
            self.catsDicts[catsIndex] = category
            XMLWiki.readXML(self.mode + '/' + category + self.oneAcapitFilename + '.xml')
            for page in XMLWiki.root.xpath('//x:page', namespaces=XMLWiki.nsmap):
                id = page[1].text
                text = page[2].text
                if id in idArraySepareteArticle:
                    continue
                sentences = json.loads(text)
                self.textsArray.extend(sentences)
                self.labelsArray.extend([catsIndex]*len(sentences))
            catsIndex += 1
        # print len(self.textsArray)

        w2v = word2vec.Word2Vec(self.textsArray, workers=self.num_workers, size=self.num_features, min_count=self.min_word_count, window=self.context, sample = self.downsampling)

        """ min_count -> odpowiada za to ze tyle razy przynajmnieij dane slowo usi wystapic w przeciwnym wypadku sa ignorowane
        size -> of the NN layers
        workers -> procesory """
        # print w2v
        # print w2v.most_similar("piotr")
        # print w2v.most_similar(positive=[u'piotr', u'strażak'], negative=[u'policjant'])
        #
        # w2v.train(lemmaGensimArr, total_examples=len(lemmaGensimArr), epochs=10)
        # print w2v
        # print w2v.most_similar(u"piotr")
        # print w2v.most_similar(positive=[u'piotr', u'strażak'], negative=[u'policjant'])

        # print w2v
        # print w2v.most_similar(u"borówka")
        # print w2v[u"borówka"]
        # print set(w2v.wv.index2word)
        # print w2v.most_similar("kolor")
        # print w2v.most_similar(positive=["kolor"])
        # print w2v.most_similar(positive=['komórka', 'kolor'])
        # print w2v.most_similar(positive=['komórka', 'kolor'], negative=[u'nie']) #tylko ze nie w stopwordsach


        # w2v.train(lemmaGensimArr, total_examples=len(lemmaGensimArr), epochs=45)
        # print "x"
        # print w2v
        # print w2v.most_similar("komórka")
        # print w2v.most_similar("kolor")
        # print w2v.most_similar(positive=["kolor"])
        # print w2v.most_similar(positive=['komórka', 'kolor'])

        w2v.save('2018/word2vec/w2v.word2vec')
        return w2v
        # modelLoaded = w2v.load('gensimModels/w2v.word2vec')

    def avgw2v(self, w2v, textArr=[]):
        """oblcizanie sredniej"""
        # w2v = word2vec.Word2Vec(lemmaGensimArr, workers=self.num_workers, size=self.num_features, min_count=self.min_word_count,
        #                         window=self.context, sample=self.downsampling)
        w2v = word2vec.Word2Vec.load('2018/word2vec/w2v.word2vec')

        if textArr != []:
            self.textsArray=textArr

        # reviewFeatureVecs = np.zeros((len(self.textsArray), self.num_features), dtype="float32")
        reviewFeatureVecs = []
        i=0

        # Index2word is a list that contains the names of the words in
        # the model's vocabulary. Convert it to a set, for speed
        self.index2word_set = set(w2v.wv.index2word)
        print "Liczba słów w2v: ", len(self.index2word_set)
        labelsArray = []
        labelsArrayNN = []
        liczbaKategorii = len(self.categories)
        print textArr
        for sentenceArr in self.textsArray:
            review = self.makeFeatureVec(sentenceArr, w2v, self.num_features)
            if review is not None:
                # reviewFeatureVecs[i] = review
                reviewFeatureVecs.append(review)
                if textArr == []:
                    labelsArray.append(self.labelsArray[i])
                    arr = [0] * liczbaKategorii
                    arr[self.labelsArray[i]]=1
                    labelsArrayNN.append(arr)
            i += 1

        # print"a"
        print len(reviewFeatureVecs),len(labelsArray),len(labelsArrayNN)
        # print labelsArrayNN, self.labelsArray
        return np.array(reviewFeatureVecs), labelsArray, labelsArrayNN
        # from sklearn.ensemble import RandomForestClassifier
        # forest = RandomForestClassifier(n_estimators=100)
        # print reviewFeatureVecs, np.zeros(len(lemmaGensimArr), dtype="int")
        # print "Fitting a random forest to labeled training data..."
        # forest = forest.fit(reviewFeatureVecs, np.zeros(len(lemmaGensimArr), dtype="bool"))


    def makeFeatureVec(self, sentenceArr, w2v, num_features):
        featureVec = np.zeros((num_features,), dtype="float32")
        #
        nwords = 0.

        # Loop over each word in the review and, if it is in the model's
        # vocaublary, add its feature vector to the total
        for word in sentenceArr:
            if word in self.index2word_set:
                nwords = nwords + 1.
                featureVec = np.add(featureVec, w2v[word])

        #
        # Divide the result by the number of words to get the average
        featureVec = np.divide(featureVec, nwords)
        print featureVec;
        if np.any(np.isnan(featureVec)):
            return None
        else:
            return featureVec

    def exampled2v(self,doc_list,labels_list, filesave):
        # sentence = doc2vec.TaggedDocument(words=[u'some', u'words', u'here'], labels=[u'SENT_1'])
        # with open('resources/categoriesTable.txt', 'r') as outfile:
        #     self.categorySet = json.load(outfile)

        # it = DocIt.DocIterator(data, docLabels)
        # it = LabeledLineSentence(data, docLabels)
        it = []
        for i in range(len(labels_list)):
            it.append(doc2vec.TaggedDocument(words=doc_list[i],tags=[labels_list[i]]))
        d2v = doc2vec.Doc2Vec(size=400, window=10, min_count=3, workers=11, alpha=0.025, min_alpha=0.025)
        d2v.build_vocab(it)
        for epoch in range(11):
            print 'iteration '+str(epoch + 1)
            d2v.train(it, total_examples=len(labels_list),epochs=epoch)
            d2v.alpha -= 0.002  # decrease the learning rate
            d2v.min_alpha = d2v.alpha  # fix the learning rate, no deca

        d2v.save('2018/doc2vec/'+filesave+'.doc2vec')
        # model_loaded = Doc2Vec.load('/tmp/my_model.doc2vec')

    def crossvaldiationd2v(self):
        doc_list, labels_list = self.__prepareBase()
        from sklearn.model_selection import KFold
        import time
        # sentence = doc2vec.TaggedDocument(words=[u'some', u'words', u'here'], labels=[u'SENT_1'])
        # with open('resources/categoriesTable.txt', 'r') as outfile:
        #     self.categorySet = json.load(outfile)

        # it = DocIt.DocIterator(data, docLabels)
        # it = LabeledLineSentence(data, docLabels)
        nsplits = 20
        kf = KFold(n_splits=nsplits, shuffle=True)
        sum = 0
        labels_index={}
        ind = 0
        for category in self.categories:
            labels_index[category]=ind
            ind+=1


        # Fit the model
        print len(doc_list), len(labels_list)
        start = time.time()
        size = 200
        print size
        precisionArr = []
        recallArr = []
        f1Arr = []
        accArr = []
        countindex = 1
        for train, test in kf.split(doc_list):
            X_train = np.array(doc_list)[train]
            Y_train = np.array(labels_list)[train]
            X_test = np.array(doc_list)[test]
            Y_test = np.array(labels_list)[test]

            it = []
            for i in range(len(Y_train)):
                it.append(doc2vec.TaggedDocument(words=X_train[i], tags=[Y_train[i]]))

            d2v = doc2vec.Doc2Vec(size=size, window=10, min_count=self.min_word_count, workers=8, alpha=0.025, min_alpha=0.025)
            d2v.build_vocab(it)
            for epoch in range(12):
                # print 'iteration ' + str(epoch + 1)
                d2v.train(it, total_examples=len(Y_train), epochs=epoch)
                d2v.alpha -= 0.002  # decrease the learning rate
                d2v.min_alpha = d2v.alpha  # fix the learning rate, no deca

            matrix = np.zeros((5,5))
            for i in range(len(X_test)):
                odczytArr = []
                for word in X_test[i]:
                    # print word
                    if word in d2v.wv.index2word:
                        odczytArr.append(d2v[word])
                if odczytArr != []:
                    matrix[labels_index[d2v.docvecs.most_similar(odczytArr)[0][0]]][labels_index[unicode(Y_test[i])]]+=1

                    # if d2v.docvecs.most_similar(odczytArr)[0][0] == unicode(Y_test[i]):
                    #     pass
            # print matrix
            TP = np.diag(matrix)
            FP = np.sum(matrix, axis=0) - TP
            FN = np.sum(matrix, axis=1) - TP
            num_classes = len(self.categories)
            TN = []
            for i in range(num_classes):
                temp = np.delete(matrix, i, 0)  # delete ith row
                temp = np.delete(temp, i, 1)  # delete ith column
                TN.append(np.sum(np.sum(temp)))
            precision = TP / (TP + FP) + 0.000001
            recall = TP / (TP + FN) + 0.000001
            f1 = 2 * (recall * precision) / (recall + precision)
            acc = (TP+TN)/(TP+TN+FP+FN)
            print countindex , precision, recall,f1, acc
            precisionArr.append(np.mean(precision))
            recallArr.append(np.mean(recall))
            f1Arr.append(np.mean(f1))
            accArr.append(np.mean(acc))
            if countindex == 7:
                break
            countindex +=1
        end = time.time()



        print "results: ", round(np.mean(precisionArr)*100,2), round(np.mean(recallArr)*100,2), round(np.mean(f1Arr)*100,2), round(np.mean(accArr)*100,2)
        print "czas:", round(end - start,2)


        # model_loaded = Doc2Vec.load('/tmp/my_model.doc2vec')


    def fit(self,filesave):
        if self.mode == "doc2vec":
            doc_list, labels_list = self.__prepareBase()
            self.exampled2v(doc_list, labels_list, filesave)

    def __prepareBase(self):
        labelArray = []
        textArray = []
        for category in self.categories:
            with open(self.folder +"/" +self.mode+"/"+category + self.oneAcapitFilename+'.txt', 'r') as outfile:
                print "wczytuje: ",self.folder +"/" +self.mode+"/"+category + self.oneAcapitFilename+'.txt'
                textArrTmp = json.load(outfile)
                labelArray.extend([category]*len(textArrTmp))
                textArray.extend(textArrTmp)
        return textArray, labelArray

    def work(self, fileFrom, textsArray):
        d2v = doc2vec.Doc2Vec.load(self.folder +"/" +self.mode+"/"+fileFrom+".doc2vec")

        for words in textsArray:
            odczytArr=[]
            print words
            for word in words:
                try:
                    odczytArr.append(d2v[word])
                except:
                    pass
            # wynik =d2v.docvecs.most_similar(odczytArr)
            # print wynik
            print d2v.most_similar([u'komórka']),'a'
            print d2v[u'komórka'],'b'
            print d2v.docvecs.most_similar([d2v[u"białko"],d2v[u'komórka']]),'c'
            return wynik
        #

if __name__ == '__main__':
    import csv
    article = Article()
    lematyzacjaClass = LematizationClass(False)
    textsArray = []
    # text = article.readText('resources/png2pdf.pdf')
    # textsArray.append(lematyzacjaClass.lemmatization(lematyzacjaClass.removeChars(text)))
    textsArray = [u'komórka']
    print textsArray
    folder = '2018'
    # categories = [u'rośliny owocowe']
    # categories = [u'biologia komórki', u'biologia rozwoju', u'histologia', u'morfologia (biologia)', u'ewolucja']
    categories = [u'komórki', u'budowa ziemi', u'wiertnictwo', u'sole organiczne', u'naczynia']
    mode = "word2vec"
    mode = "doc2vec"
    filesave = "PirwszaProba"
    fileFrom = "PirwszaProba"
    for minAppearFeature in (3,):
        for oneAcapit in (True,):
            for reperezentacjaCech in (200,):
                gensimClass = GensimClass(folder, mode, categories, True, minAppearFeature, size=reperezentacjaCech)
                gensimClass.fit(filesave)
                wynik = gensimClass.work(fileFrom, textsArray)
                with open(r'wynikiEX22x.csv', 'a') as f:
                    writer = csv.writer(f)
                    writer.writerow(
                        ['doc2vec', str(minAppearFeature), str(oneAcapit), reperezentacjaCech,
                         'NN', wynik[0][0].encode('cp1250')])
    # gensimClass.preparew2v([["piotr", "być", "strażak"],["piotr", "być", "policjant"],["strażak", "być", "sam"],["strażak", "uratował", "dom", "piotr"]])
    # gensimClass.exampled2v(['adsfsdf sadf asdf sadfsa','asdfasdfasdfadsf'],['1','2'])