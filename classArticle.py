# -*- coding: utf8 -*-
import os
import os.path
from classLematization import LematizationClass
from wand.image import Image as Imagee
from PIL import Image
from pytesseract import *
import re

import copy
import time

class Article(object):

    def __init__(self):


        self.filenameFull, self.file_name, self.file_extension, self.text = '', '', '', ''

    def readText(self, filename):
        self.filenameFull = filename
        self.text = ''
        if not os.path.exists(self.filenameFull):
            print "Plik nie istnieje"
            return False
        self.file_name, self.file_extension = os.path.splitext(self.filenameFull)
        if self.file_extension == '.pdf':
            self.__readFromPDF()
        elif self.file_extension in ('.png', '.jpg', '.jpeg', '.gif'):
            self.__ExtIMG = '.png'
            self.__readFromImage(1, True)
        elif self.file_extension in ('.txt',):
            self.__readFromTxt()
        elif self.file_extension in ('.docx',):
            self.__readFromDocx()
        # elif self.file_extension in ('.doc',):
        #     import textract
        #     text = textract.process(self.filenameFull)
        #     print text
        return self.text

    def __readFromPDF(self):
        self.__ExtIMG = '.png'
        with Imagee(filename=self.filenameFull, resolution=(300, 300)) as img:
            img.format = self.__ExtIMG[1:]
            img.type = 'grayscalematte'
            img.save(filename=self.file_name+self.__ExtIMG)
        strony, jedna_strona = self.__existing_files(self.file_name+self.__ExtIMG)
        self.__readFromImage(strony, jedna_strona)

    def __readFromImage(self, strony, jedna_strona):
        for strona in range(strony):
            if not jedna_strona:
                fileNameOCR = self.file_name + '-' + str(strona) + self.__ExtIMG
            elif jedna_strona:
                fileNameOCR = self.file_name + self.__ExtIMG
            # Image.open(fileNameOCR).save(fileNameOCR, dpi=(300, 300))

            self.text += image_to_string(Image.open(fileNameOCR).convert('RGB'), lang='pol+eng')
            os.remove(fileNameOCR)

    def __readFromTxt(self):
        with open(self.filenameFull, 'r') as myfile:
            self.text = myfile.read()

    def __readFromDocx(self):
        import docx
        doc = docx.Document(self.filenameFull)
        fullText = []
        for para in doc.paragraphs:
            fullText.append(para.text)
        self.text = '\n'.join(fullText)

    def __existing_files(self, filename_con):
        ifjedna_strona = None
        if os.path.exists(filename_con):
            ifjedna_strona = True
            strony = 1
            print "jedna strona"
        elif os.path.exists(self.file_name + '-0' + self.__ExtIMG):
            strony = 1
            ifjedna_strona = False
            while 1 == 1:
                if os.path.exists(self.file_name + '-' + str(strony) + self.__ExtIMG):
                    strony = strony + 1
                else:
                    break
            print str(strony) + "stron"
        else:
            print "plik nie istnieje"
        return strony, ifjedna_strona


    def readArticleAndModify(self, path):
        text = self.readText(path)
        textArray = self.removeChars(text)
        textArrayLast = self.lemmatization(textArray)
        return textArrayLast


if __name__ == '__main__':
    article = Article()
    lematyzacjaClass = LematizationClass(False)
    # lematyzacjaClass = LematizationClass(True)
    # article.readText('test.pdf')
    # article.readText('xsa.txt')
    text = article.readText('resources/png2pdf.pdf')
    # text = u"być bądźcie bądźże byłybyście jest są niebędącego"
    print text
    textArray = lematyzacjaClass.removeChars(text)
    print textArray,'x'
    textArrayLast = lematyzacjaClass.lemmatization(textArray)
    print textArrayLast,'y'

