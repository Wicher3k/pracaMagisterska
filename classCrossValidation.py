# -*- coding: utf-8 -*-
import time

from classMachineLearning import MachineLearning
from classesXMLLearning import XMLTokenizationIntegerWords

from sklearn import svm
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from XMLFirst import XMLWikipedia
import copy as cp
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from classesXMLLearning import XMLTokenizationBooleanWords
import numpy as np
from sklearn.model_selection import KFold, cross_val_score
from classGensim import GensimClass
from f1metricNN import f1NN,precisionNN,recallNN
from sklearn.model_selection import ShuffleSplit
from sklearn.model_selection import cross_validate

class CrossValidationClass(MachineLearning):

    def __init__(self, categories, folder='', model='', extendedCSV=True, onlySeparableCats=True, minAppearFeature=0, oneAcapit=False, rC="int"):
        """
        :param categories: (array) jakie kategorie maja byc brane do nauki
        :param extendedCSV: (boolean) - true generuje powiekszony raport slow o ilosc wystapein w kazdej kategorii oraz procentowe obliczenie w stosunku do wsyzstkich slow
        """
        self.cechyArray = {}
        self.categories = categories
        self.extendedCSV = extendedCSV
        self.subfolder = model
        self.folder = folder
        self.onlySeparableCats = onlySeparableCats
        self.minAppearFeature = minAppearFeature
        self.oneAcapit = oneAcapit
        self.rC = rC
        if oneAcapit:
            self.oneAcapitFilename = "_oneAcapit"
        else:
            self.oneAcapitFilename = ""
        if self.subfolder in ('word2vec','doc2vec'):
            self.gensimClass = GensimClass(folder=self.folder, mode=self.subfolder, categories=self.categories,
                                      oneAcapit=self.oneAcapit, minAppearFeature=minAppearFeature)
            self.w2vmodel = None

    def __prepereFeatures(self):
        super(CrossValidationClass, self).prepereFeatures(self.categories, "CrossValidation")  # przygotowanie do nauczania

    # def __separeteArticlesMultiCategory(self):
    #     XMLWiki = XMLWikipedia(self.folder)
    #     XMLWiki.loadCategoriesTable()
    #     for category in self.categories:  # trzeba wykonowyac przed kazdym nauczaniem (z powodu mozliwosci artykulow wystepujacych w kilku kategoriach, a robimy tylko rozlaczne)
    #         XMLWiki.readXML(self.subfolder + '/' + category + '.xml')
    #         XMLWiki.export(['wordsNumberCSV'], category, onlySeparableCats=True, categoriesArr=cp.copy(categories))
    #     return XMLWiki.idArraySepareteArticle

    def prepareBasesToFit(self):
        """Prepare bases to fit"""
        print"\tprepareBasesToFit"
        idArraySepareteArticle = self._separeteArticlesMultiCategory()
        print "idArraySepareteArticle: " + str(len(idArraySepareteArticle))
        if self.subfolder in ('lematyzacja', 'slowosiec', "lematyzacja+slowosiec"):
            self.__prepereFeatures()
            if self.rC == "int":
                XML = XMLTokenizationIntegerWords(self.categories, self.folder, self.subfolder, self.cechyArray, idArraySepareteArticle, oneAcapit=self.oneAcapit, minAF=self.minAppearFeature)
                print"\tXMLBayesSave"
                XML.XMLBayesSave()
            elif self.rC == "bool":
                XML = XMLTokenizationBooleanWords(self.folder, self.subfolder, self.cechyArray, idArraySepareteArticle,
                                                  oneAcapit=self.oneAcapit, minAF=self.minAppearFeature)
                XML.neuralNetworkVectors(self.categories)
        elif self.subfolder in ('word2vec'):
            self.w2vmodel = self.gensimClass.preparew2v(idArraySepareteArticle)

    def crossValidationd2v(self):
        self.gensimClass.crossvaldiationd2v()


    def crossValidation(self):
        if self.subfolder in ('doc2vec'):
            self.crossValidationd2v()
        else:
            if self.subfolder in ('lematyzacja', 'slowosiec', "lematyzacja+slowosiec"):
                if self.rC == "int":
                    XML = XMLTokenizationIntegerWords(self.categories, self.folder, self.subfolder, self.cechyArray, [], oneAcapit=self.oneAcapit, minAF=self.minAppearFeature)
                    npFeaturesArray, npCategoriesArrayNN, npCategoriesArray = XML.naiveBayesPrepereBase()
                elif self.rC == "bool":
                    XML = XMLTokenizationBooleanWords(self.folder, self.subfolder, self.cechyArray, [],
                                                      oneAcapit=self.oneAcapit, minAF=self.minAppearFeature)
                    npFeaturesArray, npCategoriesArrayNN, npCategoriesArray = XML.neuralNetworkPrepereBase(self.categories)

            elif self.subfolder in ('word2vec'):
                if self.w2vmodel is not None:
                    npFeaturesArray, npCategoriesArray, npCategoriesArrayNN = self.gensimClass.avgw2v(self.w2vmodel)
                else:
                    print "Brak w2vmodel"


            print len(npCategoriesArray),"Liczba cech:", len(npFeaturesArray[0]), len(npFeaturesArray[1])


            nb_class = GaussianNB()
            rf_class = RandomForestClassifier(n_estimators=20, n_jobs=-1)
            kneigh_class = KNeighborsClassifier(n_neighbors=11, n_jobs=-1)
            log_class = LogisticRegression(n_jobs=-1)


            cv = ShuffleSplit(n_splits=20)
            # print("Naive Bayes: ")
            # # start = time.time()
            # x = cross_validate(nb_class, npFeaturesArray, npCategoriesArray, scoring={'prec':'precision_macro','recall':'recall_macro','f1':'f1_macro','acc':'accuracy'}, cv=cv, return_train_score=False)
            # # end = time.time()
            # print(x)
            # prec = x['test_prec'].mean() * 100
            # recall = x['test_recall'].mean() * 100
            # f1s = x['test_f1'].mean() * 100
            # acc = x['test_acc'].mean() * 100
            # timeall = x['fit_time'].sum()
            # print "Results of Naive Bayes are: ", round(prec,2), round(recall,2), round(f1s,2), round(acc,2) #Macro averaging is conventionally performed over each of p, r and for separately, so f is an average of harmonic means, not a harmonic mean itself
            # print "Execution time: (prawdziwy)",  round(timeall,2)
            #
            #
            # print("\n\nRandom Forests: ")
            # # start = time.time()
            # x = cross_validate(rf_class, npFeaturesArray, npCategoriesArray,
            #                    scoring={'prec': 'precision_macro', 'recall': 'recall_macro', 'f1': 'f1_macro',
            #                             'acc': 'accuracy'}, cv=cv, return_train_score=False)
            # # end = time.time()
            # print(x)
            # prec = x['test_prec'].mean() * 100
            # recall = x['test_recall'].mean() * 100
            # f1s = x['test_f1'].mean() * 100
            # acc = x['test_acc'].mean() * 100
            # timeall = x['fit_time'].sum()
            # print("Results of Random Forests is: ", round(prec,2), round(recall,2), round(f1s,2), round(acc,2))
            # print "Execution time: (prawdziwy)", round(timeall,2)
            # del x, nb_class, rf_class

            # print("\n\nLog:")
            # # start = time.time()
            # x = cross_validate(log_class, npFeaturesArray, npCategoriesArray,
            #                    scoring={'prec': 'precision_macro', 'recall': 'recall_macro', 'f1': 'f1_macro',
            #                             'acc': 'accuracy'}, cv=cv, return_train_score=False)
            # # end = time.time()
            # print(x)
            # prec = x['test_prec'].mean() * 100
            # recall = x['test_recall'].mean() * 100
            # f1s = x['test_f1'].mean() * 100
            # acc = x['test_acc'].mean() * 100
            # timeall = x['fit_time'].sum()
            # print("Results of Log is: ", round(prec,2), round(recall,2), round(f1s,2), round(acc,2))
            # print "Execution time: (prawdziwy)", round(timeall, 2)

            # print("\n\nKNeighborsClassifier:")
            # # start = time.time()
            # x = cross_validate(kneigh_class, npFeaturesArray, npCategoriesArray,
            #                    scoring={'prec': 'precision_macro', 'recall': 'recall_macro', 'f1': 'f1_macro',
            #                             'acc': 'accuracy'}, cv=cv, return_train_score=False)
            # # end = time.time()
            # print(x)
            # prec = x['test_prec'].mean() * 100
            # recall = x['test_recall'].mean() * 100
            # f1s = x['test_f1'].mean() * 100
            # acc = x['test_acc'].mean() * 100
            # timeall = x['fit_time'].sum()
            # print("Results of KNeighborsClassifier is: ", round(prec,2), round(recall,2), round(f1s,2), round(acc,2))
            # print "Execution time: (prawdziwy)", round(timeall, 2)


            nsplits = 20
            kf = KFold(n_splits=nsplits, shuffle=True)
            sum = 0

            # Fit the model
            print len(npFeaturesArray), len(npCategoriesArrayNN)
            f1scores = []
            accscores = []
            prescores = []
            recscores = []
            start = time.time()
            i=1
            for train, test in kf.split(npFeaturesArray):
                X_train = np.array(npFeaturesArray)[train]
                Y_train = np.array(npCategoriesArrayNN)[train]
                X_test = np.array(npFeaturesArray)[test]
                Y_test = np.array(npCategoriesArrayNN)[test]
                model = Sequential()
                model.add(
                    Dense(len(npFeaturesArray[0]) / 20, input_dim=len(npFeaturesArray[0]), kernel_initializer='uniform',
                          activation='relu'))
                # model.add(Dropout(0.5))
                # model.add(Dense(self.lenFeatures/8, kernel_initializer='uniform', activation='sigmoid', use_bias=True))
                model.add(Dense(len(self.categories), kernel_initializer='uniform', activation='sigmoid'))
                model.compile(loss='binary_crossentropy',
                              optimizer='rmsprop', metrics=[precisionNN,recallNN,f1NN,'accuracy'])
                model.fit(X_train, Y_train, epochs=22, batch_size=10, verbose=0, shuffle=True)
                scores = model.evaluate(X_test, Y_test)
                print scores
                prescores.append(scores[1] * 100)
                recscores.append(scores[2] * 100)
                f1scores.append(scores[3] * 100)
                accscores.append(scores[4] * 100)
                if i == 7:
                    break
                i+=1
            end = time.time()
            print "\n\nSieci Average: metric", round(np.mean(prescores),2), round(np.mean(recscores),2),round(np.mean(f1scores),2), round(np.mean(accscores),2)
            print "Execution time:  ", round(end - start,2)
            print "\n\n"



            # nsplits = 10
            # kf = KFold(n_splits=nsplits, shuffle=True)
            # sum = 0
            # clf = GaussianNB()
            # for train, test in kf.split(npFeaturesArray):
            #     X_train = np.array(npFeaturesArray)[train]
            #     Y_train = np.array(npCategoriesArray)[train]
            #
            #     X_test = np.array(npFeaturesArray)[test]
            #     Y_test = np.array(npCategoriesArray)[test]
            #     clf.fit(X_train, Y_train)
            #     procent = clf.score(X_test, Y_test) * 100
            #     print procent, '\n'
            #     sum += procent
            #
            # average = sum / nsplits
            # print "NB Average:", average





if __name__ == '__main__':

    # categories = [u'biologia komórki', u'biologia molekularna', u'biologia rozrodu', u'statystyka opisowa', u'rośliny owocowe']
    # categories = [u'rośliny owocowe',u'biologia komórki', u'biologia molekularna']
    # categories = [u'biologia komórki', u'biologia rozwoju', u'histologia', u'morfologia (biologia)', u'ewolucja']
    categories = [u'komórki', u'budowa ziemi', u'wiertnictwo', u'sole organiczne', u'naczynia']
    # categories = [u'rośliny owocowe']
    folder = '2018'
    # model = 'lematyzacja'
    # model = 'slowosiec'
    # model = "lematyzacja+slowosiec"
    model = "word2vec"
    # model = "doc2vec"

    # minAppearFeature = 3
    # oneAcapit = True
    # reperezentacjaCech = "bool"
    # # reperezentacjaCech = "int"
    # print"Zaczynam: ", model
    # print "minAppearFeature: ", minAppearFeature
    # print "oneAcapit: ", oneAcapit
    # print "reperezentacjaCech: ", reperezentacjaCech
    # classNB = CrossValidationClass(categories, folder, model, False, onlySeparableCats=True,
    #                                minAppearFeature=minAppearFeature, oneAcapit=oneAcapit, rC=reperezentacjaCech)
    # # classNB.prepareBasesToFit()
    # classNB.crossValidation()
    # print"\n-------------------------Koniec------------------------\n\n"
    #
    minAppearFeature = 3
    oneAcapit = True
    # reperezentacjaCech = "bool"
    reperezentacjaCech = "int"
    print"Zaczynam: ", model
    print "minAppearFeature: ", minAppearFeature
    print "oneAcapit: ", oneAcapit
    print "reperezentacjaCech: ", reperezentacjaCech
    classNB = CrossValidationClass(categories, folder, model, False, onlySeparableCats=True,
                                   minAppearFeature=minAppearFeature, oneAcapit=oneAcapit, rC=reperezentacjaCech)
    classNB.prepareBasesToFit()
    classNB.crossValidation()
    time.sleep(30)
    print"\n-------------------------Koniec------------------------\n\n"

    # minAppearFeature = 3
    # oneAcapit = False
    # reperezentacjaCech = "bool"
    # # reperezentacjaCech = "int"
    # print"Zaczynam: ", model
    # print "minAppearFeature: ", minAppearFeature
    # print "oneAcapit: ", oneAcapit
    # print "reperezentacjaCech: ", reperezentacjaCech
    # classNB = CrossValidationClass(categories, folder, model, False, onlySeparableCats=True,
    #                                minAppearFeature=minAppearFeature, oneAcapit=oneAcapit, rC=reperezentacjaCech)
    # classNB.prepareBasesToFit()
    # classNB.crossValidation()
    #
    # print"\n-------------------------Koniec------------------------\n\n"
    # time.sleep(30)
    #
    # minAppearFeature = 3
    # oneAcapit = False
    # # reperezentacjaCech = "bool"
    # reperezentacjaCech = "int"
    # print"Zaczynam: ", model
    # print "minAppearFeature: ", minAppearFeature
    # print "oneAcapit: ", oneAcapit
    # print "reperezentacjaCech: ", reperezentacjaCech
    # classNB = CrossValidationClass(categories, folder, model, False, onlySeparableCats=True,
    #                                minAppearFeature=minAppearFeature, oneAcapit=oneAcapit, rC=reperezentacjaCech)
    # classNB.prepareBasesToFit()
    # classNB.crossValidation()
    #
    # print"\n-------------------------Koniec------------------------\n\n"
    # time.sleep(30)

    # minAppearFeature = 0
    # oneAcapit = True
    # reperezentacjaCech = "bool"
    # # reperezentacjaCech = "int"
    # print"Zaczynam: ", model
    # print "minAppearFeature: ", minAppearFeature
    # print "oneAcapit: ", oneAcapit
    # print "reperezentacjaCech: ", reperezentacjaCech
    # classNB = CrossValidationClass(categories, folder, model, False, onlySeparableCats=True,
    #                                minAppearFeature=minAppearFeature, oneAcapit=oneAcapit, rC=reperezentacjaCech)
    # classNB.prepareBasesToFit()
    # classNB.crossValidation()
    # print"\n-------------------------Koniec------------------------\n\n"
    # #
    # minAppearFeature = 0
    # oneAcapit = True
    # # reperezentacjaCech = "bool"
    # reperezentacjaCech = "int"
    # print"Zaczynam: ", model
    # print "minAppearFeature: ", minAppearFeature
    # print "oneAcapit: ", oneAcapit
    # print "reperezentacjaCech: ", reperezentacjaCech
    # classNB = CrossValidationClass(categories, folder, model, False, onlySeparableCats=True,
    #                                minAppearFeature=minAppearFeature, oneAcapit=oneAcapit, rC=reperezentacjaCech)
    # classNB.prepareBasesToFit()
    # classNB.crossValidation()
    # # time.sleep(30)
    # print"\n-------------------------Koniec------------------------\n\n"

    # minAppearFeature = 0
    # oneAcapit = False
    # reperezentacjaCech = "bool"
    # # reperezentacjaCech = "int"
    # print"Zaczynam: ", model
    # print "minAppearFeature: ", minAppearFeature
    # print "oneAcapit: ", oneAcapit
    # print "reperezentacjaCech: ", reperezentacjaCech
    # classNB = CrossValidationClass(categories, folder, model, False, onlySeparableCats=True,
    #                                minAppearFeature=minAppearFeature, oneAcapit=oneAcapit, rC=reperezentacjaCech)
    # classNB.prepareBasesToFit()
    # classNB.crossValidation()
    #
    # print"\n-------------------------Koniec------------------------\n\n"
    # time.sleep(30)
    #
    # minAppearFeature = 0
    # oneAcapit = False
    # # reperezentacjaCech = "bool"
    # reperezentacjaCech = "int"
    # print"Zaczynam: ", model
    # print "minAppearFeature: ", minAppearFeature
    # print "oneAcapit: ", oneAcapit
    # print "reperezentacjaCech: ", reperezentacjaCech
    # classNB = CrossValidationClass(categories, folder, model, False, onlySeparableCats=True,
    #                                minAppearFeature=minAppearFeature, oneAcapit=oneAcapit, rC=reperezentacjaCech)
    # classNB.prepareBasesToFit()
    # classNB.crossValidation()
    #
    # print"\n-------------------------Koniec------------------------\n\n"
