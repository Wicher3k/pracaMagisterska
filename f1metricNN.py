# -*- coding: utf-8 -*-
from keras import backend as K


def recallNN(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())
    return recall


def precisionNN(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision

def f1NN(y_true, y_pred):
    precision = precisionNN(y_true, y_pred)
    recall = recallNN(y_true, y_pred)
    return 2*((precision*recall)/(precision+recall+K.epsilon()))