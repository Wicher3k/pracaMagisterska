# -*- coding: utf-8 -*-

from lxml import etree, objectify
import re, json, codecs, numpy as np
import copy as cp

from classXMLManager import XMLManager

class XMLWikipedia(XMLManager):

    def __init__(self,folder=''):
        """ Inicjalizacja zmiennych """
        XMLManager.__init__(self, folder)
        self.nsmap = {'x': 'http://www.mediawiki.org/xml/export-0.10/'}
        self.filePathCats, self.treeCats, self.rootCats = '', '', ''
        self.categorySet, self.categoryDict, self.categoryNone = [], {}, set()
        self.title2Redirect = {}
        self.ns, self.nsThing = [], {}
        self.categoryMap = None
        self.idArraySepareteArticle=[]

    def readXMLCats(self, filepath):
        """ Odczyt z pliku oraz zmienne z kategorii"""
        print u"\t Wczytywanie pliku "+(self.folder+filepath)
        self.filePathCats = self.folder+filepath
        self.treeCats = etree.parse(self.filePathCats)
        self.rootCats = self.treeCats.getroot()
        objectify.deannotate(self.rootCats, cleanup_namespaces=True)

    def clearAndSafeCats(self, newFile = ''):
        """ Zapis do pliku Cats"""
        if newFile != '':
            self.filePathCats = self.folder+newFile
        print u"\t Zapis " + self.filePathCats
        objectify.deannotate(self.rootCats, cleanup_namespaces=True)
        tree = etree.ElementTree(self.rootCats)
        try:
            tree.write(self.filePathCats, xml_declaration=True, pretty_print=True, encoding="utf-8")
        except:
            pass

    def deleteAllWithoutPages(self, array2Delete):
        """ Usunięcie wszystkich niepotrzebnych tagów w strikturze XML"""
        print u"\t Usunięcie tagów"
        for tag in array2Delete:
            for element in self.root.xpath('//x:'+tag, namespaces=self.nsmap):
                element.getparent().remove(element)

    def deletetagesWithArguments(self,dictTagsAndArguments={}):
        """ Usunięcie wszystkich niepotrzebnych tagów o podanych argumentach gdzie key1 to tag, key2 to nazwa argumentu a value to array wartosci argumentow"""
        print u'\t Usuniecie tagow o danych wartosciach argumentow'
        for tag in dictTagsAndArguments:
            for argument in dictTagsAndArguments[tag]:
                for valueArr in dictTagsAndArguments[tag][argument]:
                    for element in self.root.xpath('//'+tag+'[@'+argument+'>="'+str(valueArr[0])+'" and @'+argument+'<="'+str(valueArr[1])+'"]'):
                        element.getparent().remove(element)

    def deleteArgumentsInTags(self,dictTagAndArgumentsArr):
        """ Usunięcie wszystkich niepotrzebnych argumentow w danych tagach"""
        print u'\t Usuniecie argumentow'
        for tag in dictTagAndArgumentsArr:
            for element in self.root.xpath(tag):
                 for argiument in dictTagAndArgumentsArr[tag]:
                    try:
                        del element.attrib[argiument]
                    except:
                        pass

    def deleteIdInRevision(self):
        """ Usunięcie wszystkich tagów 'id' w tagu 'revision'"""
        for element in self.root.xpath('//x:revision/x:id', namespaces=self.nsmap):
            element.getparent().remove(element)

    def tagtext2parent(self):
        """ Przeniesienie tagu text wyzej co umożliwia skasowanie tagu revision"""
        print u"\tUsuwam revision"
        for text in self.root.xpath('//x:text', namespaces=self.nsmap):
            revision = text.getparent()
            page = text.getparent().getparent()
            page.append(text)
            page[4].text = text.text
            revision.getparent().remove(revision)


    def deleteEmptyArticles(self):
        """ Usuwa Artykuly z pustymi tagami text"""
        print u"\t Usunięcie pusty artykułów"
        for text in self.root.xpath('//x:text', namespaces=self.nsmap):
            if text.text is None:
                element = text.getparent()
                element.getparent().remove(element)

    def deleteNotArticles(self):
        """ Usunięcie nie artykułów i nie kategorii"""
        print u"\t Usunięcie nie-artykułów i nie-kategorii"
        for ns in self.root.xpath('//x:ns', namespaces=self.nsmap):
            if ns.text not in ('0', '14'):
                element = ns.getparent()
                element.getparent().remove(element)

    def deleteRedirects(self):
        """ Odpowiada za skasowanie artykułów z redirectami i 'patrz' oraz dodaje do zmiennej tytuł artykułu i tytuł artykułu na które wskazuje przekierowanie"""
        print u"\t Moduł przekierowan"
        for text in self.root.xpath('//x:text', namespaces=self.nsmap):
            if text.text.lower().lstrip().startswith(('#redirect', '#patrz', '#przekieruj', '#tam')): # lstrip usuwa biale znaki na poczatku stringu (spacje, nowe linie, tabulatory)
                element = text.getparent()
                titleDestination = self.__find_between(text.text, '[[', ']]')
                if titleDestination not in self.title2Redirect:
                    self.title2Redirect[titleDestination] = element[0].text
                else:
                    self.title2Redirect[titleDestination] += '|' + element[0].text
                element.getparent().remove(element)

    def __find_between(self, s, first, last):
        """ Zwraca wyraz pomiędzy danymi znakami"""
        try:
            start = s.index(first) + len(first)
            end = s.index(last, start)
            return s[start:end]
        except ValueError:
            return ""

    def deletePagesUjednoznaczenie(self):
        """ Usuwwa stronu ujednoznacznienie """
        print u"\t Usuwam ujednoznaczenia"
        for text in self.root.xpath('//x:text', namespaces=self.nsmap):
            if text.text.lower().startswith('{{ujednoznacznienie'):
                element = text.getparent()
                element.getparent().remove(element)

    def deleteRedundantTextInArticles(self):
        print u"\t Usuwam zbędny tekst w artykułach UZUPELNIC nazwy obok(bibliografia, linki zewnęrtrzne, adnotacje, wiersze z 'sortuj' , 'wikibooks',''wikisłownik', 'przypisy 'plik i jego nazwę' {{Kontrola autorytatywna}}"
        i = 0
        for text in self.root.xpath('//x:text', namespaces=self.nsmap):
            # page = text.getparent()
            # if page[0].text==u"Kategoria:Polskie diecezje rzymskokatolickie":
            # if i in (48,49,50):
            text.text = self.savecategory(unicode(text.text))
            # print type(text.text)
            text.text = self.deleteInStylesTags(text.text,
                                                ['code', 'math', 'sub', 'SUB', 'sup', 'syntaxhighlight', 'source',
                                                 'ref', 'timeline', 'imagemap'])
            text.text = self.deleteInStylesAttr(text.text,
                                                ['group', 'color', 'style', 'style ', 'width', 'height', 'class', 'bgcolor', 'colspan'])

            text.text = self.deleteWords(text.text, u'.png|.JPG|.jpg|sortable|wikitable|text-align|scriptstyle|class=|align=|https=|{{medal}}|{{Kontrola autorytatywna}}|<blockquote>|</blockquote>|<gallery'+
                                                     u'|color=|=blue|=green|=brown|url=|http=|=small|=large|=right|=left|font=|</gallery>|Plik:|<center>|</center>|{{Zastrzeżenia|<br />|text=|Commonscat|{{commons'+
                                                     u'|<span >|</span>|wikitable|cellpadding=|cellspacing=|margin=|rowspan=|=clear|{{zobacz też|{{uwagi}}|{{Uwagi|uwagi=|<u>'+
                                                     u'|<b>|<i>|</u>|</i>|</b>|accessdate|inne znaczenia|{{spis treści}}|== Zobacz też ==|ujednoznacznienie|border=|nbsp')
            text.text = self.deleteSections(text.text,
                                            [' Bibliografia ', u' Linki zewnętrzne ', u'Linki zewnętrzne',
                                             ' Adnotacje ', ' Przypisy '])  # mmozna dac: u' Zobacz też ' (nie bo sa tam wazne infromacje usuwam jedynie samo zobacz tez
            text.text = self.deleteBetween(text.text, ['sortuj', 'wikibooks', 'sub', u'wikisłownik', u'wikiźródła'])
            text.text = self.deletePrzypisy(self.deleteLang(self.deleteFile(text.text)))
            text.text = self.deleteDefaultsort(self.deleteSize(self.deleteDopracowac(text.text)))
            text.text = self.deleteFirstInfo(self.deletePixels(self.deleteMapaLokalizacyjna(text.text)))
            # print '\n\n\n',text.text,'\n\n\n\n\n\n'
            if i % 10000 == 0:
                print 'nr: ', i
            i += 1
            # break

    def savecategory(self, text):
        return re.sub('\[\[Kategoria', '\n[[Kategoria', text, flags=re.IGNORECASE)

    def deleteFile(self, text):
        return re.sub(r'\[\[plik:.*?\|', '', text, flags=re.IGNORECASE)

    def deleteLang(self, text):
        return re.sub(r'{{Lang.*?\n', '', text, flags=re.IGNORECASE)

    def deletePrzypisy(self, text):
        return re.sub(r'{{Przypisy.*?\n', '', text, flags=re.IGNORECASE)

    def deleteDopracowac(self, text):
        return re.sub(u'{{dopracować.*?\n', '', text, flags=re.IGNORECASE)

    def deleteSize(self, text):
        return re.sub(u'size.*?px', '', text, flags=re.IGNORECASE)

    def deleteDefaultsort(self, text):
        return re.sub(u'{{DEFAULTSORT.*?\n', '', text, flags=re.IGNORECASE)

    def deleteMapaLokalizacyjna(self, text):
        return re.sub(r'{{mapa lokalizacyjna.*?}}', '\n', text, flags=re.DOTALL)

    def deleteFirstInfo(self, text):
        return re.sub(r"^{{.*?}}", '\n', text, flags=re.DOTALL)

    def deletePixels(self, text):
        return re.sub(r"([0-9].)(px|mm)", '', text, flags=re.IGNORECASE)

    def deleteWords(self, text, wordsTxt):

        text = re.sub(wordsTxt, '', text, flags=re.IGNORECASE)
        return text

    def deleteSections(self, text, sectionsArray):
        for section in sectionsArray:
            text = re.sub(u'==' + section + u'==.*?\n\n', '\n', text, flags=re.DOTALL)
        return text
        # try:
        #     print re.findall(r'== Bibliografia ==.*?\n\n', text, flags=re.DOTALL)[0]
        # except:
        #     print "brak"

    def deleteBetween(self, text, elementsArray):
        for element in elementsArray:
            text = re.sub(r'{{' + element + '.*?}}', '', text, flags=re.IGNORECASE)
        return text

    def deleteInStylesAttr(self, text, attrArray):
        for attr in attrArray:
            text = re.sub(r'' + attr + '=".*?"', '', text, flags=re.IGNORECASE)
        return text

    def deleteInStylesTags(self, text, tagsArray):
        for tag in tagsArray:
            text = re.sub(r'<' + tag + '.*?(</' + tag + '>|/>)', '\n', text, flags=re.DOTALL)
        return text

    # def deleteWords(self, text, wordsArray):
    #     for word in wordsArray:
    #         text = text.replace(word, '')
    #     return text

    # !!! na koncu trzeba bedzie, usunac "border",
    # !!! a takze znaki |! zamienic na spacje, oraz "<liczba>" na puste


    # def deleteInStylesTagsCat(self, tag):
    #     for ns in self.root.xpath('//x:ns', namespaces=self.nsmap):
    #         if ns.text == '14':
    #             page = ns.getparent()
    #             text = page[3][0]
    #             text.text = self.deleteInStylesTags(text.text, [tag])

    def changeCharsTitle(self):
        """ Podiana pauzy na jednego rodzaju"""
        for title in self.root.xpath('//x:title', namespaces=self.nsmap):
            title.text = title.text.replace(u'–', u'-').replace(u'—', u'-')

    def buildCategorySet(self):
        """ buduje tablice kategorii"""
        print u"\t Zebranie tablicy kategorii"
        for ns in self.root.xpath('//x:ns', namespaces=self.nsmap):
            if ns.text == '14':
                title = ns.getparent()[0]
                self.categorySet.append(title.text[10:].lower())  # dodanie kategorii

    def saveRedirects(self):
        print u"Zapis redirectow do pliku"
        if self.title2Redirect != {}:
            with open('resources/redirects.txt', 'w+') as outfile:
                json.dump(self.title2Redirect, outfile)

    def saveCategoriesTable(self):
        print u"Zapis tablicy kategorii do plików"
        if self.categorySet != []:
            self.__sortAndDictCategory()
            with open('resources/categoriesTable.txt', 'w+') as outfile:
                json.dump(self.categorySet, outfile)

            with open('resources/categoriesDict.txt', 'w+') as outfile:
                json.dump(self.categoryDict, outfile)

    def __sortAndDictCategory(self):
        self.categorySet.sort()
        for i in range(len(self.categorySet)):
            self.categoryDict[self.categorySet[i]] = i
        # print  self.categoryDict

    def deleteArticlesPages(self):
        """ Usunięcie artykułów by zostały same kategorie"""
        print u"Usuwanie artykułów"
        for ns in self.root.xpath('//x:ns', namespaces=self.nsmap):
            if ns.text == '0':
                element = ns.getparent()
                # print element[2].text
                element.getparent().remove(element)

    def deleteCategoriesPages(self):
        """  Funkcja usuwa tylko takie artykuly które sa kategoriami. """
        print u"\t Usuwam artykuły - kategorie"
        for ns in self.root.xpath('//x:ns', namespaces=self.nsmap):
            if ns.text == '14':
                element = ns.getparent()
                # print element[2].text
                element.getparent().remove(element)


    def bindFiles(self, filepath):
        """ Scalanie plików"""
        print u'\tbindFiles '+self.folder+filepath
        self.filePathBind = self.folder+filepath
        self.treeBind = etree.parse(self.filePathBind)
        self.rootBind = self.treeBind.getroot()
        objectify.deannotate(self.rootBind, cleanup_namespaces=True)
        for page in self.rootBind.xpath('//x:page', namespaces=self.nsmap):
            self.root.append(page)

    #!!! Funkcje z drugiego pliku

    def loadRedirects(self):
        """ Wczytanie redirectow do zmiennej"""
        with open('resources/redirects.txt', 'r') as outfile:
            self.title2Redirect = json.load(outfile)

    def loadCategoriesTable(self):
        print u"Odczyt tablicy kategorii z plików"
        with open('resources/categoriesTable.txt', 'r') as outfile:
            self.categorySet = json.load(outfile)
        with open('resources/categoriesDict.txt', 'r') as outfile:
            self.categoryDict = json.load(outfile)
        self.categoryMap = np.zeros((len(self.categorySet), len(self.categorySet)), dtype=np.bool)
        self.numerCategory = [0] * len(self.categorySet)
        # print len(self.categoryMap),len(self.categoryMap[0])


    def buildCategoryMapCats(self):
        """ buduje mapę kategorii, plik włącząć tylk odla plik uz kategoriami"""
        print u"\t Zebranie mapy kategorii"
        # print len(self.categoryMap), len(self.categoryMap[0])
        for page in self.rootCats.xpath('//x:page', namespaces=self.nsmap):
            title = page[0]
            titletext = title.text[10:].lower()
            text = page[2]
            # print text
            categoriesArrayParentNr = self.__getCategoriesFromText(text)
            categoriesElementNr = self.__getIdOfCategory(titletext)
            page.append(etree.Element("categoryTrace")) #dodanie nowego tagu
            self.__setAttribute(page[3], 'category', categoriesElementNr)

            # print categoriesArrayParentNr, titletext, categoriesElementNr

            for categorieParent in categoriesArrayParentNr:
                # print categoriesElementNr,categorieParent
                self.categoryMap[categoriesElementNr][categorieParent] = True
        # print len(self.categoryMap),'liczba kategorii'

    def setCategoriesCats(self):
        """ Ustawia ID kategorii """
        print u"\t Ustawienie kategorii artykułów"
        i = 0
        for text in self.rootCats.xpath('//x:text', namespaces=self.nsmap):
            page = text.getparent()
            categoriesArray = self.__getCategoriesFromText(text)
            allCategories = self.__findTrackCategory(categoriesArray,[self.__getIdOfCategory(page[0].text[10:].lower())])
            self.__saveToCategoryTrace(page[3], sorted(allCategories))
            if i % 10000 == 0:
                print 'artykul', i
            i += 1

    def searchDistanceBetweenCategories(self, categoriesArr):
        """Tutaj bedzie obliczana odleglsoc miedzy kategoriami. Prawdopodobnie bede bral nuemr kategorii i bede szedl w góre kategoriami. za pomoca zmiennej selfcategoryMap"""
        print u"\t searchDistanceBetweenCategories"
        if len(categoriesArr)>1:
            categoriesMatrixesPath = []
            categoriesIdsPath = []
            for category in categoriesArr:
                for title in self.rootCats.xpath(u'//x:title[text()="Kategoria:'+category+u'"]', namespaces=self.nsmap):
                    matrixCat = {}
                    page = title.getparent()
                    categoriesIdArr = json.loads(page[3].text)
                    startCategoryId = page[3].get('category')
                    for categoryId in categoriesIdArr:
                        categoriesParent = np.where(self.categoryMap[categoryId] == True)[0]
                        matrixCat[categoryId] = {}
                        matrixCat[categoryId]['distance'] = 0 #inicjacja
                        matrixCat[categoryId]['parent'] = list(categoriesParent)

                    self.__setDistance(int(startCategoryId), matrixCat, 1)
                    categoriesIdsPath.append(set(categoriesIdArr))
                    categoriesMatrixesPath.append(matrixCat)
                    # categoriesParent = np.where(self.categoryMap[category] == True)
            if len(categoriesIdsPath) < 2:
                print"error"
            thesameSet = categoriesIdsPath[0].intersection(categoriesIdsPath[1])
            length = 1000
            foundCategory = ""
            for idCat in thesameSet:
                lengthtemp = categoriesMatrixesPath[0][idCat]['distance']+categoriesMatrixesPath[1][idCat]['distance']
                if lengthtemp < length:
                    length = lengthtemp
                    foundCategory = idCat
            print length,self.__getCategoryOfID(foundCategory)
        else:
            print "Mala liczba kategorii"
        # print len(self.categoryNone)

    def __setDistance(self, childId, matrixCat, distance = 1):
        for parendId in matrixCat[childId]['parent']:
            matrixCat[parendId]['distance'] = distance
            self.__setDistance(parendId, matrixCat, distance+1)


    def __findTrackCategory(self, categoriesArray, allCategories):
        """znalezienie sciezki kategorii od najszczegolniejszej do ogolnej"""
        for category in categoriesArray:
            if category not in allCategories:
                # print self.__getCategoryOfID(category)
                allCategories.append(int(category))
                # print category, self.__getCategoryOfID(category), self.__getIdOfCategory(u'Języki programowania')
                # self.categoryMap[category][67646] = 1
                # print self.categoryMap[category][57646], self.categoryMap[57646][category]
                categoryParent=np.where(self.categoryMap[category] == True)
                # print category,categoryParent,categoryParent[0],len(categoryParent[0]),allCategories
                if len(categoryParent[0]) > 0:
                    allCategories = self.__findTrackCategory(categoryParent[0], allCategories)
        return allCategories

    def __getCategoriesFromText(self,text):
        """ Z artykulu zwraca jego kategorie"""
        ifCategoryArray = re.split('[\[\]]', text.text[max(text.text.lower().find('[[kategoria:'), 0):].strip())  # https://stackoverflow.com/questions/37932006/print-everything-after-a-few-specific-characters-in-a-string i użyte tegular expressions
        # print ifCategoryArray,"a"
        categoriesArray = self.__leaveOnlyCategories(ifCategoryArray)
        # print categoriesArray,text.getparent()[1].text, "b"
        # print categoriesArray
        for j in range(2):
            categoriesArray = self.__removeRedundantchars(categoriesArray)
        # print categoriesArray, "c"
        self.__removeRedundantcharsDash(categoriesArray)
        self.__multiCategoriesNEW(categoriesArray)
        self.__getIdOfCategoryArray(categoriesArray)
        return categoriesArray

    def __getIdOfCategoryArray(self, categoriesArray):
        for i in range(len(categoriesArray)):
            categoriesArray[i] = self.__getIdOfCategory(categoriesArray[i])
        while None in categoriesArray:
            categoriesArray.remove(None)

    def __getIdOfCategory(self, categoryName):
        try:
            return self.categoryDict[categoryName]
        except:
            # self.categoryNone.add(categoryName)
            return None

    def __getCategoryOfID(self, id):
        try:
            return self.categorySet[id]
        except:
            # self.categoryNone.add(categoryName)
            return None

    def __leaveOnlyCategories(self, ifCategoryArray):
        categories = []
        for i in range(len(ifCategoryArray)):
            if ifCategoryArray[i][:10].lower() == 'kategoria:':
                if ifCategoryArray[i][10:] != '':
                    categories.append(ifCategoryArray[i][10:].lower())  # lower
        return categories

    def __removeRedundantchars(self, categoriesArray):
        for i in range(len(categoriesArray)):
            categoriesArray[i] = categoriesArray[i].replace('&nbsp;', '').replace('basepagename', '').replace('subpagename', '').replace('pagename', '') #zmienic na lower
            if categoriesArray[i][-1] in ('*', ' ', '|', '!', '{', '}'):
                categoriesArray[i] = categoriesArray[i][:-1]
            if categoriesArray[i][0] in ('*', ' ', '!'):
                categoriesArray[i] = categoriesArray[i][1:]
        return categoriesArray

    def __removeRedundantcharsDash(self, categoriesArray):
        for i in range(len(categoriesArray)):
            categoriesArray[i] = categoriesArray[i].replace(u'–', u'-').replace(u'—', u'-')

    def __multiCategoriesNEW(self, categoriesArray):
        toDel = []
        for i in range(len(categoriesArray)):
            if ('|' or ',') in categoriesArray[i]:
                subcategories = (categoriesArray[i].replace(',', '|').split('|'))
                categoriesArray.append(subcategories[0])
                toDel.append(i)
        for dele in sorted(toDel, key=int, reverse=True):
            categoriesArray.pop(dele)

    def __saveToCategoryTrace(self, categoryTrace, categoriesArray):
        """Zapis tekstu sciezke kategorii"""
        categoryJson = json.dumps(categoriesArray)
        # print text, categoriesArray, categoryJson
        categoryTrace.text = categoryJson

    def setCategoryTracesArticles(self):
        """ Dodanie nowego tagu Categorie i zapis wszystkich kategorii dla tego artykulu"""
        print u'\t setCategoryTracesArticles'
        categoriesAll = {}
        for categoryTrace in self.rootCats.xpath('//x:categoryTrace', namespaces=self.nsmap):
            categoriesAll[int(categoryTrace.get('category'))] = json.loads(categoryTrace.text)
        # del self.filePathCats
        # del self.treeCats
        # del self.rootCats
        import copy as cp
        i = 0
        for text in self.root.xpath('//x:text', namespaces=self.nsmap):
            page = text.getparent()
            categoriesArray = self.__getCategoriesFromText(text)
            # print categoriesArray
            try:
                page[3]
            except:
                page.append(etree.Element("categories"))  # dodanie nowego tagu
                self.__setAttribute(page[3], 'articleCategory', categoriesArray)
            categoriesTraces = cp.copy(categoriesArray)
            for category in categoriesArray:
                categoriesTraces.extend(categoriesAll[category])
            page[3].text = json.dumps(list(set(categoriesTraces)))
            if i % 10000 == 0:
                print 'artykul', i
            i += 1

    def deleteNonCategoriesArticles(self):
        print u'Usuwam artykuły bez kategorii'
        self.nonCategoriesTitle = []
        i = 0
        for categories in self.root.xpath('//x:categories', namespaces=self.nsmap):
            # print categories.get('articleCategory')
            if categories.get('articleCategory') == '[]':
                element = categories.getparent()
                # print element[0].text
                self.nonCategoriesTitle.append(element[0].text)
                element.getparent().remove(element)
                i+=1
        # print i

        with open('resources/noncategoriesArticles.txt', 'w+') as outfile:
            json.dump(self.nonCategoriesTitle, outfile)

    def __setAttribute(self, tag, nameattr, value):
        valueJson = json.dumps(value)
        tag.attrib[nameattr] = valueJson

    def drowCatsPath(self, titlestr, option, type):
        """ 
        :param titlestr: tytuł artykułu
        :param option: [article, category] - root ścieżki
        :param type: [reingold_tilford, sugiyama ] - algorytmy tworzenia ścieżki
        :return: 
        """
        print "\t rysuje sciezke"
        import igraph as ig
        categories = "brak"
        for title in self.root.xpath('//x:title', namespaces=self.nsmap):
            if title.text == titlestr:
                categoriesArticle = json.loads(title.getparent()[3].get('articleCategory'))
                categories = json.loads(title.getparent()[3].text)
                break
        if categories == "brak":
            print 'blad w tym pliku brak'
            return
        categories = list(set(categories))
        numberCats = len(categories)
        # print categories,
        names, pairs, color, pairs2 = [], [], [], []
        for cat in categories:
            nazwa = self.__getCategoryOfID(cat)
            # print cat, nazwa
            names.append(nazwa.encode('utf8', 'replace'))
            if nazwa == 'kategorie':
                x = categories.index(cat)
            if nazwa == 'kategorie' or cat in categoriesArticle:
                color.append('green')
            else:
                color.append('red')

        if option == 'article':
            mode = "out"
            names.append((u'Artykuł:' + titlestr).encode('utf8', 'replace'))
            color.append('green')
            for category in categoriesArticle:
                pairs.append((numberCats, categories.index(category)))
                pairs2.append((categories.index(category), numberCats))
            root = numberCats
            numberCats += 1
        elif option == 'category':
            mode = "in"
            root = x
        else:
            print "nie ma takiego"
        # print "x",categories
        for category in categories:
            categoriesParent = np.where(self.categoryMap[category] == True)
            # print "y",categoriesParent,
            for categoryParent in categoriesParent[0]:
                # print (categories.index(category), categories.index(categoryParent))
                pairs.append((categories.index(category), categories.index(categoryParent)))
                pairs2.append((categories.index(categoryParent), categories.index(category)))
        graph = ig.Graph(n=numberCats, directed=True)
        visual_style = {}
        # print type
        if type == 'sug': #sugiyama
            # print"jestem"
            graph.add_edges(pairs2)
            visual_style["layout"] = graph.layout_sugiyama()
        elif type == 'rei': #reingold_tilford
            # print"jestem2"
            graph.add_edges(pairs)
            visual_style["layout"] = graph.layout_reingold_tilford(mode=mode, root=[root])

        visual_style["bbox"] = (3000, 2000)
        visual_style["bbox"] = (500, 650)
        # visual_style["bbox"] = (1300, 1100)
        visual_style["margin"] = 40
        graph.vs["label"] = names
        graph.vs["color"] = color
        ig.plot(graph, 'resources/SciezkaKategorii' + option + titlestr + type + '.png', **visual_style)

    def countArticlesOfCategory(self):
        """Zliczam artykuły kategorii - potrzebne do exportu"""
        print u'\tZliczam artykuły kategorii'
        # i = 0
        for categories in self.root.xpath('//x:categories', namespaces=self.nsmap):
            try:
                categoriesArray = json.loads(categories.text)
                # print categoriesArray
            except:
                continue
                # i += 1
                # if i in (3,):
                # print categories.getparent()[0].text,categories.getparent()[1].text,categories.getparent()[2].text,'\n\n\n\n'
                # break
            for category in set(categoriesArray):
                # print self.__getCategoryOfID(category)
                self.numerCategory[category] += 1
                # i += 1
                # print "\n\n"
                # if i ==10:
                #     break

    def export(self, array2save, category='', onlySeparableCats=True, categoriesArr=[]):
        if 'categoryCSV' in array2save:
            self.countArticlesOfCategory()
            print "zapis listy kategorii"
            f = codecs.open(u"resources/Kategorie.csv", encoding='cp1250', mode="wb", errors='ignore')
            f.write(u"Kategoria;Liczba\n")
            # i=0
            for id in range(len(self.numerCategory)):
                f.write(self.__getCategoryOfID(id) + u';' + unicode(self.numerCategory[id]) + '\n')
                # if i%100==0:
                # print i
                # i+=1

        if 'wordsNumberCSV' in array2save:
            if onlySeparableCats:
                self.__getIdOfCategoryArray(categoriesArr)
            else:
                categoriesArr=[category]
            self.countWordsIn(categoriesArr)
            f = codecs.open(u"resources/Words"+category+".csv", encoding='cp1250', mode="wb", errors='ignore')
            f.write(u"Słowo;Liczba wystąpień\n")
            for word in self.wordsDict:
                f.write(unicode(word) + u';' + unicode(self.wordsDict[word]) + '\n')

    def countWordsIn(self, categoriesIdArr=[]):
        self.wordsDict = {}
        for page in self.root.xpath('//x:page', namespaces=self.nsmap):
            categoriesTag = page[3]
            categoriesArticle = json.loads(categoriesTag.text)
            text = page[2]
            count = 0
            for catId in categoriesIdArr:
                if catId in categoriesArticle:
                    count += 1
            if count != 1:
                # print page[1].text,page[0].text
                self.idArraySepareteArticle.append(page[1].text)
                continue

            words = json.loads(text.text)
            if not isinstance(words[0], list):
                for word in words:
                    try:
                        self.wordsDict[word] += 1
                    except:
                        self.wordsDict[word] = 1
            else:
                for word in words:
                    for wordx in word:
                        try:
                            self.wordsDict[wordx] += 1
                        except:
                            self.wordsDict[wordx] = 1



    def setRedirects(self): #  funckjcja do poprawy
        print u"\t Dodanie Redirectów "
        for title in self.root.xpath('//x:title', namespaces=self.nsmap):
            try:
                # print title.getparent()[2].text+'\n' + self.title2Redirect[title.text]
                title.getparent()[2].text += '\n' + self.title2Redirect[title.text]
                # print title.getparent()[2].text
                # break
            except:
                continue
            # print self.title2Redirect[title.text],title.text,title.getparent()[1].text


    def divideIntoCategoriesFiles(self, categoriesArray, oneAcapit=False):
        """
        :param categoriesArray: tablica kategorii
        :param oneAcapit: boolean - jezeli true to bierze tlko jeden akapit glowny, false - caly tekst
        :return: 
        """
        if oneAcapit:
            addFilename="_oneAcapit"
        else:
            addFilename=""
        # print categoriesArray
        save = {}
        self.__getIdOfCategoryArray(categoriesArray)
        print categoriesArray,'x'
        for categoryName in categoriesArray:
            save[categoryName] = etree.Element("metatag")
        # print save
        for categoriesTag in self.root.xpath('//x:categories', namespaces=self.nsmap):
            # categoriesArticle = json.loads(categoriesTag.get('articleCategory')) #gdybysm ychieli tylk otaki e co faktycnzie tylk odo tej kategorii należą
            categoriesArticle = json.loads(categoriesTag.text)
            # print 'x',categoriesArray,categoriesArticle
            for catId in categoriesArray:
                if catId in categoriesArticle:
                    # print catId, categoriesTag.getparent()[0].text.lower()  # Transport aktywny
                    if oneAcapit:
                        text = categoriesTag.getparent()[2].text
                        tytul = categoriesTag.getparent()[0].text.lower()
                        tytulWithotBrackets = re.sub(' \([^\)]+\)', '', tytul)
                        # print self.__getCategoryOfID(catId), tytulWithotBrackets, u"\'\'\'\[\["+tytulWithotBrackets
                        suchen = re.search(r'\'\'\''+tytulWithotBrackets+r'.*?\n\n|\'\'\'\[\['+tytulWithotBrackets+r'.*?\n\n|'+tytulWithotBrackets+r'\'\'\'.*?\n\n|'+tytulWithotBrackets+r'\]\]\'\'\'.*?\n\n', text.lower(), flags=re.DOTALL)
                        # print suchen
                        if suchen:
                            categoriesTag.getparent()[2].text = suchen.group(0)
                        else:

                            suchen2 = re.search(r'\'\'\'' + tytul[:3] + r'.*?\n\n|\'\'\'\[\[' + tytul[:3] + r'.*?\n\n',
                                               text.lower(), flags=re.DOTALL)
                            # print "x"
                            if suchen2:
                                print "&&&w zuchen2!&&&"
                                categoriesTag.getparent()[2].text = suchen2.group(0)
                            else:
                                # print text,catId, categoriesTag.getparent()[0].text.lower()
                                # print repr(text)
                                print "***nie znalazlem paragrafu!***"
                                continue
                    save[catId].append(cp.copy(categoriesTag.getparent()))
        for catId in categoriesArray:
            self.safeNewXml(self.__getCategoryOfID(catId)+addFilename + '.xml', save[catId])

    def safeNewXml(self, filename, root):
        print u"\t Zapis " + filename
        objectify.deannotate(root, cleanup_namespaces=True)
        tree = etree.ElementTree(root)
        try:
            tree.write(self.folder+filename, xml_declaration=True, pretty_print=True, encoding="utf-8")
        except:
            pass




    ### INNE ###


    def numberOfArticles(self):
        """ Pobranie liczby artykułów"""
        print len(self.root)

    def countTagsPage(self):
        """ Służy do wyłąpania tagów zbędnych """
        for page in self.root.xpath('//x:page', namespaces=self.nsmap):
            if len(page.getchildren()) != 3:
                print page[1].text, len(page.getchildren()), 'z'

    def countTagsRevision(self):
        """ Służy do wyłąpania tagów zbędnych """
        for revision in self.root.xpath('//x:revision', namespaces=self.nsmap):
            if len(revision.getchildren()) != 1:
                print len(revision.getchildren()), 'x'

    def findArticle(self, titlestr):
        for title in self.root.xpath('//x:title', namespaces=self.nsmap):
            # print type(title.text), type(titlestr)
            if title.text == titlestr:
                # print title.text
                # print title.getparent()[2].text
                # try:
                #     # print title, title.getparent()[1].text, title.getparent()[2].text
                #     print title.text, '\n', title.getparent()[2].text
                #     # print repr(title.getparent()[2].text)
                # except:
                #     print title, title.getparent()[1].text, title.getparent()[3].text
                #     print repr(title.getparent()[3].text)
                with open(u'resources/'+titlestr+'.txt', 'w+') as outfile:
                    outfile.write(title.getparent()[2].text.encode('utf8'))
                    # self.noncategoriesArticles = json.load(outfile)
                    # print len(self.noncategoriesArticles)
        print u"Zapisano do: "+u'resources/'+titlestr+'.txt'

    def checkNamesNotArticles(self):
        """ Sprawdza jakie sa przedrostki w tekstach"""
        arrayNames = []
        for title in self.root.xpath('//x:title', namespaces=self.nsmap):
            if u':' in title.text:
                pos = title.text.index(u':')
                if title.text[:pos] not in arrayNames:
                    arrayNames.append(title.text[:pos])
        print arrayNames

    def ns14(self):
        """ sprawdza wszytskie mozliwe page (ns) """
        for ns in self.root.xpath('//x:ns', namespaces=self.nsmap):
            if ns.text not in self.ns:
                self.ns.append(ns.text)
                self.nsThing[ns.text] = ns.getparent()[0].text
        print self.ns
        print self.nsThing

    def ns2600(self):
        """ zlicza ilość ns 2600 - Wątki """
        i = 0
        for ns in self.root.xpath('//x:ns', namespaces=self.nsmap):
            if ns.text == '2600':
                i += 1
                print i

    def addCategorytag(self):
        """ dodanie tagu jakbym musiał """
        self.root.insert(0, etree.Element("child0"))

    def readnonCatlen(self):
        with open('resources/noncategoriesArticles.txt', 'r+') as outfile:
            self.noncategoriesArticles = json.load(outfile)
            print len(self.noncategoriesArticles)

    def returnCatsFromCatPage(self, pageNameCat):
        """Zwraca kategorie danej Kategorii"""
        for title in self.rootCats.xpath('//x:title', namespaces=self.nsmap):
            if title.text == pageNameCat:
                categories = json.loads(title.getparent()[3].text)
                break
        self.returnCatsFromCategoriesArray(categories)

    def returnCatsFromArticlePage(self, pageNameArticle):
        categories = ""
        for title in self.root.xpath('//x:title', namespaces=self.nsmap):
            if title.text == pageNameArticle:
                categories = json.loads(title.getparent()[3].text)
                break
        if categories=="":
            print"nie znaleziono"
            return
        categoriesReturn = self.returnCatsFromCategoriesArray(categories)
        return categoriesReturn

    def returnCatsFromCategoriesArray(self, categoriesArray):
        # print len(categoriesArray), categoriesArray
        categoriesReturn = []
        # i = 0
        for categoryTrace in self.rootCats.xpath('//x:categoryTrace', namespaces=self.nsmap):
            if int(categoryTrace.get('category')) in categoriesArray:
                print categoryTrace.getparent()[0].text, int(categoryTrace.get('category'))
                categoriesReturn.append(int(categoryTrace.get('category')))
                # i += 1
        # print i
        return categoriesReturn




""" Poniższe odpalenie skryptu spowoduje wczytanie wszystkich czterech plików Wikipedi a następnie zmiane struktury XMLA wykasowanie niepotrzebnych pozycji oraz tekstu"""
if __name__ == '__main__':
    print "XMLFirst"
    folder = "2018"
    # import time
    # start = time.time()
    XML = XMLWikipedia(folder)
    for nr in range(1, 5):
        print "Inicjalizacja pliku nr: ", nr
        XML.readXML('plwiki'+str(nr)+'.xml')
        #XML.readXML('sss.xml')

        # Funkcje do usuniecia poczatkowyc hzzbednych rzeczy i inicjalizacja
        XML.deleteAllWithoutPages(['comment', 'contributor', 'format', 'minor', 'model', 'parentid', 'timestamp', 'siteinfo', 'sha1', 'redirect', 'restrictions'])
        XML.deleteIdInRevision()
        XML.tagtext2parent()
        XML.deleteNotArticles()
        XML.deleteEmptyArticles()
        XML.deleteRedirects()
        XML.deletePagesUjednoznaczenie()
        XML.deleteRedundantTextInArticles()
        XML.changeCharsTitle()
        XML.buildCategorySet()
        # #
        # # # XML.findArticle(u'Kategoria:Polskie diecezje rzymskokatolickie')
        # XML.numberOfArticles()
        XML.clearAndSafe()
    """   ### Funkcje w celach testowania
    # #     # XML.numberOfArticles()
    # #     # XML.getTextId(234926)
    # #     # XML.countTagsPage()
    # #     # XML.countTagsRevision()
    #     XML.findArticle(u'Kardioonkologia')
    #
    # #     # XML.checkNamesNotArticles()
    # #     # XML.ns14()
    # #     # XML.ns2600()
    # #
    # #     ### funkcje do skasowania"""

    XML.saveRedirects()
    XML.saveCategoriesTable()

    # # """ Tworzenie plików"""
    for nr in range(1, 5):
        print nr
        XML.readXML('plwiki' + str(nr) + '.xml')
        XML.deleteArticlesPages()
        XML.deleteAllWithoutPages(['ns'])
        XML.clearAndSafe('plwikiCat' + str(nr) + '.xml')

    for nr in range(1, 5):
        print nr
        XML.readXML('plwiki' + str(nr) + '.xml')
        XML.deleteCategoriesPages()
        # XML.numberOfArticles()
        XML.deleteAllWithoutPages(['ns'])
        XML.clearAndSafe('plwikiArticles' + str(nr) + '.xml')

    # #
    # """Scalenie do jednego kategorii"""
    nr=1
    print nr
    XML.readXML('plwikiCat' + str(nr) + '.xml')
    for nr in range(2, 5):
        XML.bindFiles('plwikiCat' + str(nr) + '.xml')
    XML.clearAndSafe('plwikiCats.xml')
    #
    #
    """Scalenie do jednego pliku artykułów"""
    nr=1
    print nr
    XML.readXML('plwikiArticles' + str(nr) + '.xml')
    for nr in range(2, 5):
        XML.bindFiles('plwikiArticles' + str(nr) + '.xml')

    XML.clearAndSafe('plwikiArticlesX.xml')