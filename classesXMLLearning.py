# -*- coding: utf-8 -*-
import json,numpy as np
from XMLFirst import XMLWikipedia
import os.path

class XMLTokenizationBooleanWords(XMLWikipedia):

    def __init__(self, folder='', subfolder='', featuresArray='',idArraySepareteArticle=[], oneAcapit=False,minAF=0):
        if featuresArray == ' ':
            print "nie podano cech"
        self.featuresArray = featuresArray
        if subfolder != '':
            self.subfolder = subfolder+'/'
        else:
            self.subfolder = subfolder
        if oneAcapit:
            self.oneAcapitFilenameAF = "_oneAcapit"+"_"+str(minAF)
        else:
            self.oneAcapitFilenameAF = ""+"_"+str(minAF)
        if oneAcapit:
            self.oneAcapitFilename = "_oneAcapit"
        else:
            self.oneAcapitFilename = ""
        XMLWikipedia.__init__(self,folder)
        self.idArraySepareteArticle = idArraySepareteArticle

    def neuralNetworkVectors(self, categories):
        for category in categories:
            if not os.path.isfile(self.folder+'/'+self.subfolder + 'bool/bool_' + category +self.oneAcapitFilenameAF + '.xml'):
                self.readXML(self.subfolder + category+self.oneAcapitFilename  + u'.xml')
                ile = 0
                for page in self.root.xpath('//x:page', namespaces=self.nsmap):
                    id = page[1].text
                    if id in self.idArraySepareteArticle:
                        # print id
                        page.getparent().remove(page)
                        continue
                    text = page[2]
                    textArray = json.loads(text.text)
                    # print textArray
                    wordsArray = np.zeros(len(self.featuresArray), dtype='b')
                    # print wordsArray
                    # for i in range(len(self.featuresArray)):
                    #     if self.featuresArray[i] in textArray:
                    #         wordsArray[i] = 1
                    for word in set(textArray):  # mozna sproowac aby bylo szybciej robic petle po tekscie a nie cechac. ale potem wyciagać index
                        try:
                            wordsArray[self.featuresArray.index(word)] = 1  # tutaj roznica
                        except:
                            pass
                    text.text = json.dumps(wordsArray.tolist())
                    ile+=1
                print u"Liczba artykułów kategorii " + category + u" : " + unicode(ile)
                self.clearAndSafe(self.subfolder + 'bool/bool_' + category +self.oneAcapitFilenameAF + '.xml')

    def neuralNetworkPrepereBase(self, categories):
        arrFeatures, arrCategoriesNN, arrCategoriesKlasyfi, arrCategoriesSave = [], [], [], {}
        liczbaKategorii = len(categories)
        for i in range(len(categories)):
            arrCategoriesSave[i] = categories[i]
            self.readXML(self.subfolder+'bool/bool_' + categories[i] + self.oneAcapitFilenameAF + '.xml')
            # arr = np.empty([1, len(self.featuresArray)],dtype='b')
            # print arr

            for text in self.root.xpath('//x:text', namespaces=self.nsmap):
                arr = [0] * liczbaKategorii
                arr[i] = 1
                arrCategoriesNN.append(arr)
                arrFeatures.append(json.loads(text.text))
                arrCategoriesKlasyfi.append(i)

        with open('resources/naiveBayesCategories.json', 'w+') as outfile:
            json.dump(arrCategoriesSave, outfile)

        npFeaturesArray = np.asarray(arrFeatures)
        npCategoriesArray = np.asarray(arrCategoriesNN)
        npCategoriesArrayKlasi = np.asarray(arrCategoriesKlasyfi)

        return npFeaturesArray, npCategoriesArray, npCategoriesArrayKlasi

    def neuralNetworkPrepereObject(self, objectTextsArray):
        predictArray = []
        for objectTextArray in objectTextsArray:
            wordsArray = np.zeros(len(self.featuresArray), dtype='b')
            for i in range(len(self.featuresArray)):
                if self.featuresArray[i] in objectTextArray:
                    wordsArray[i] = 1
            predictArray.append(wordsArray)
        return predictArray


class XMLTokenizationIntegerWords(XMLWikipedia):

    def __init__(self, categories, folder='', subfolder='', featuresArray='',idArraySepareteArticle=[], oneAcapit=False,minAF=0):
        if featuresArray == '':
            print "nie podano cech"
        self.featuresArray = featuresArray
        # print self.featuresArray
        self.categories = categories
        if subfolder != '':
            self.subfolder = subfolder+'/'
        else:
            self.subfolder = subfolder
        if oneAcapit:
            self.oneAcapitFilenameAF = "_oneAcapit" + "_" + str(minAF)
        else:
            self.oneAcapitFilenameAF = "" + "_" + str(minAF)
        if oneAcapit:
            self.oneAcapitFilename = "_oneAcapit"
        else:
            self.oneAcapitFilename = ""
        XMLWikipedia.__init__(self, folder)
        self.idArraySepareteArticle=idArraySepareteArticle


    def XMLBayesSave(self):
        """ roznica jest taka meidzy neuralNetworkVectors take ze jest dodawana +1 a nie =1"""
        for category in self.categories:
            # print self.folder+self.subfolder+category+self.oneAcapitFilename  + u'.xml'
            # print os.path.isfile(self.folder+'/'+self.subfolder+'int/int_' + category + self.oneAcapitFilenameAF + '.xml'),'a'
            if not os.path.isfile(self.folder+'/'+self.subfolder+'int/int_' + category + self.oneAcapitFilenameAF + '.xml'):
                self.readXML(self.subfolder+category+self.oneAcapitFilename  + u'.xml')
                ile = 0
                for page in self.root.xpath('//x:page', namespaces=self.nsmap):
                    id = page[1].text
                    if id in self.idArraySepareteArticle:
                        # print id
                        page.getparent().remove(page)
                        continue
                    text = page[2]
                    textArray = json.loads(text.text)
                    wordsArray = np.zeros(len(self.featuresArray), dtype='b')
                    # for i in range(len(self.featuresArray)):
                    #     wordsArray[i] += textArray.count(self.featuresArray[i])  # tutaj roznica
                    for word in textArray:  # mozna sproowac aby bylo szybciej robic petle po tekscie a nie cechac. ale potem wyciagać index
                        try:
                            wordsArray[self.featuresArray.index(word)] += 1  # tutaj roznica
                        except:
                            pass
                    text.text = json.dumps(wordsArray.tolist())
                    ile += 1
                print u"Liczba artykułów kategorii "+category+u" : "+unicode(ile)
                self.clearAndSafe(self.subfolder+'int/int_' + category + self.oneAcapitFilenameAF + '.xml')

    def naiveBayesPrepereBase(self):
        arrFeatures, arrCategories, arrCategoriesNN, arrCategoriesSave = [], [], [], {}
        liczbaKategorii = len(self.categories)
        for i in range(len(self.categories)):
            arrCategoriesSave[i] = self.categories[i]
            self.readXML(self.subfolder+'int/int_' + self.categories[i] +self.oneAcapitFilenameAF + '.xml')
            for text in self.root.xpath('//x:text', namespaces=self.nsmap):# w featurach pobierac idiki dokumentów dla ktorych sa przynajmniej 2 kategorie i tutaj bede sprawdzac id
                arr = [0] * liczbaKategorii
                arr[i] = 1
                arrCategoriesNN.append(arr)
                arrCategories.append(i)
                arrFeatures.append(json.loads(text.text))

        with open('resources/naiveBayesCategories.json', 'w+') as outfile:
            json.dump(arrCategoriesSave, outfile)

        npFeaturesArray = np.asarray(arrFeatures)
        npCategoriesArray = np.asarray(arrCategories)
        npCategoriesNN = np.asarray(arrCategoriesNN)
        return npFeaturesArray, npCategoriesNN, npCategoriesArray

    def naiveBayesPrepereObject(self, objectTextsArray):
        predictArray = []
        for objectTextArray in objectTextsArray:
            wordsArray = np.zeros(len(self.featuresArray), dtype='b')
            for i in range(len(self.featuresArray)):
                wordsArray[i] += objectTextArray.count(self.featuresArray[i])  # tutaj roznica
            predictArray.append(wordsArray)
        return predictArray

