# -*- coding: utf-8 -*-
from XMLFirst import XMLWikipedia
from classLematization import LematizationClass

# from MachineLearning import MachineLearning
import copy as cp,json
# from classArticle import Article
from classSlowosiec import slowosiecXML
import re

class XMLModelPreparation(XMLWikipedia):

    def __init__(self, folder='', modeCechy='', oneAcapit=False):
        XMLWikipedia.__init__(self, folder)
        self.modeCechy = modeCechy
        self.lemaClass = LematizationClass(True)
        if self.modeCechy in ("doc2vec"):
            from classGensim import GensimClass
            # self.gensimClass = GensimClass(folder=folder, mode=modeCechy, categories=[],oneAcapit=oneAcapit, minAppearFeature=0)
            self.textArray = []
            self.labelArray = []

    def __textLemmatization(self, textTag):
        textArray = self.lemaClass.removeChars(textTag.text + " " + textTag.getparent()[0].text)  # + dodanie tytułu
        textArrayLast = self.lemaClass.lemmatization(textArray)
        return textArrayLast

    def textLemmatizationGensim(self, text):
        """Podzielenie tekstu ze wzgledu na kropki na arraye a potem lematyzacja kazdeg oz array.
        :return: macierz
        """
        textSentenceArr = self.lemaClass.createSenetncesArray(text)
        textArray = []
        for textSentence in textSentenceArr:
            array = self.lemaClass.lemmatization(self.lemaClass.removeChars(textSentence))
            if array != []:
                textArray.append(array)
        return textArray

    def lematizationProcess(self):
        """ 
        dla kazdego artykułu wykonuje lematyzacje oraz usuwa niepotrzebne znaki.
        :return: nadpisuje tekst artykułów tablicą słów.
        """
        print u'\tUsuwam niealfanumeryczne znaki'
        i = 0

        for textTag in self.root.xpath('//x:text', namespaces=self.nsmap):
            if i % 1000 == 0:
                print 'artykul: ', i
            textArrayLast = self.__textLemmatization(textTag)
            textTag.text = json.dumps(textArrayLast)
            i += 1


    def __textSlowosiec(self, titleTag):
        titleArray = self.lemaClass.removeChars(re.sub('\([^\)]+\)', '', titleTag.text.lower()))
        titleArray.append(re.sub('\([^\)]+\)', '', titleTag.text.lower()).strip().encode('UTF8'))  # oprocz tego ze zostalo podzielone na czesci to dajemy caly nazwe, czesto tez sa spotykane w slowosieci wieloslowa
        titleArray = self.lemaClass.lemmatization(titleArray)
        titleArray = list(set(titleArray))
        allWords = cp.copy(titleArray)
        # print title.text,allWords
        for word in titleArray:
            # slowosiecSynsetArray = self.slowoXML.getHiperonimsAndHiponimsFromWord(word, ('10', '11'))
            slowosiecSynsetArray = self.slowoXML.getPathTreeSlowosiec(word, ('10'), 2, 1, True)
            slowosiecSynsetArray2 = self.slowoXML.getPathTreeSlowosiec(word, ('11'), 2, 1, True)
            slowosiecSynsetArray2.extend(self.slowoXML.getPathTreeSlowosiec(word, ('15', '25', '26', '27', '28', '29', '65', '115', '116', '117', '3412'), 10, 1, True))
            slowosiecSynsetArray2.extend(self.slowoXML.getPathTreeSlowosiec(word, ('14', '20', '21', '22', '23', '24', '64', '112', '113', '114', '3410'), 10, 1, True))
            slowosiecSynsetArray2.extend(self.slowoXML.getPathTreeSlowosiec(word, ('30', '60', '61', '62', '63', '141', '142', '148', '169'), 200, 1, True))
            allWords.extend(self.slowoXML.mergesynsetObjectPathAndGetWords(slowosiecSynsetArray, slowosiecSynsetArray2))

        allWords = list(set(allWords))
        return allWords
        # print title.text.lower().strip().encode('UTF8'),allWords
        # print allWords


    def slowosiecProcess(self):
        """
        Pobiera tytuł artykułu, nastepnie zamiast tekstu generuje m.in. hiponimy, synonimy i hiperonimy oraz usuwa co ejst w nawiasach w tytule. czesto bywa np. xx (biologia)
        :return: 
        """
        print u'\tWykonuje procesy związane ze słowosiecia'
        i = 0
        self.slowoXML = slowosiecXML('memory')
        self.slowoXML.readXML(folder + '/plwordnetMine.xml')
        for titleTag in self.root.xpath('//x:title', namespaces=self.nsmap):
            if i % 100 == 0:
                print 'artykul: ', i
            # print titleTag.text
            allWords = self.__textSlowosiec(titleTag)
            if len(allWords) > 2:
                titleTag.getparent()[2].text = json.dumps(allWords)
            else:
                print "usuwam", titleTag.text.lower().strip().encode('UTF8')
                x = titleTag.getparent()
                x.getparent().remove(x)
            i += 1

    def lematizationSlowosiecProcess(self):
        """
        Wykonuje te same prace dla lematizationProcess oraz slowosiecProcess
        :return: 
        """
        print u'\tWykonuje procesy związane z procesami słowosieci i lematyzacji'
        i = 0
        self.slowoXML = slowosiecXML('memory')
        self.slowoXML.readXML(folder + '/plwordnetMine.xml')
        for pageTag in self.root.xpath('//x:page', namespaces=self.nsmap):
            textTag = pageTag[2]
            titleTag = pageTag[0]
            if i % 100 == 0:
                print 'artykul: ', i
            textArrayLast = self.__textLemmatization(textTag)
            allWords = self.__textSlowosiec(titleTag)
            textArrayLast.extend(allWords)
            textTag.text = json.dumps(textArrayLast)
            i += 1

    def word2vecProcess(self, categoryFilename):
        """
        Wykonuje prace zwiazana z przetwarzaniem word2vec dla kategorii
        :return: 
        """

        # i = 0
        # text = ""
        for pageTag in self.root.xpath('//x:page', namespaces=self.nsmap):
            textTag = pageTag[2]
            titleTag = pageTag[0]
            # if i % 1000 == 0:
            #     print 'artykul: ', i
            text = titleTag.text+' '+textTag.text+'. '
            textTag.text = json.dumps(self.textLemmatizationGensim(text))
            # i += 1
        # textArray = self.__textLemmatizationGensim(text)
        # with open(self.folder +self.modeCechy+'/'+ categoryFilename + '.txt', 'w+') as outfile:
        #     outfile.write(json.dumps(textArray))



    def doc2vecProcess(self):
        i = 0
        text = ""
        for pageTag in self.root.xpath('//x:page', namespaces=self.nsmap):
            textTag = pageTag[2]
            titleTag = pageTag[0]
            if i % 100 == 0:
                print 'artykul: ', i
            text += titleTag.text+' '+textTag.text+'. '
            # textTag.text = json.dumps(textArrayLast)
            i += 1

        textArray = self.textLemmatizationGensim(text)
        self.labelArray.extend([category] * len(textArray))
        self.textArray.extend(textArray)
        return textArray


    def doc2vecSafe(self, textArray, newFile):
        with open(self.folder + newFile[:-4]+'.txt', 'w+') as outfile:
            json.dump(textArray, outfile)

"""tutaj tworzone sa pliki poszczegolnych kategorii"""
if __name__ == '__main__':
    print "XMLLematization"
    folder = '2018'
    XML = XMLWikipedia(folder)

    # array = ['biofizyka', 'curling', 'cukrownictwo']
    # array = ['biofizyka']
    # categories = [u'biologia komórki', u'biologia molekularna', u'biologia rozrodu', u'statystyka opisowa',
    #               u'rośliny owocowe']
    categories = [u'rośliny owocowe',u'biologia molekularna',u'biologia komórki']
    categories = [u'biologia komórki',u'biologia rozwoju',u'histologia',u'morfologia (biologia)',u'ewolucja']
    # categories = [u'komórki', u'budowa ziemi', u'wiertnictwo', u'sole organiczne', u'naczynia']
    # modeCechy = "lematyzacja"
    # modeCechy = "slowosiec"
    # modeCechy = "lematyzacja+slowosiec"
    modeCechy = "word2vec"
    # modeCechy = "doc2vec"
    # modeCechy = ""
    #
    oneAcapit=True
    oneAcapit=False
    XML.loadCategoriesTable()
    XML.readXML('plwikiArticlesX.xml')
    XML.divideIntoCategoriesFiles(cp.copy(categories), oneAcapit=oneAcapit)
    del XML

    #
    XMLPreparation = XMLModelPreparation(folder, modeCechy,oneAcapit)
    if oneAcapit:
        addFilename = "_oneAcapit"
    else:
        addFilename = ""
    for category in categories:
        XMLPreparation.readXML(category+addFilename+'.xml')
        if modeCechy == "lematyzacja":
            XMLPreparation.lematizationProcess()
            # XML.findArticle(u'Brachiacja')
            XMLPreparation.clearAndSafe(modeCechy+'/'+category+addFilename+'.xml')

        elif modeCechy == "slowosiec":
            XMLPreparation.slowosiecProcess()
            XMLPreparation.clearAndSafe(modeCechy+'/' + category+addFilename + '.xml')

        elif modeCechy == "lematyzacja+slowosiec":
            XMLPreparation.lematizationSlowosiecProcess()
            XMLPreparation.clearAndSafe(modeCechy + '/' + category+addFilename + '.xml')

        elif modeCechy == "word2vec":
            XMLPreparation.word2vecProcess(category+addFilename)
            XMLPreparation.clearAndSafe(modeCechy + '/' + category+addFilename + '.xml')

        elif modeCechy == "doc2vec":
            textArr = XMLPreparation.doc2vecProcess()
            XMLPreparation.doc2vecSafe(textArr, modeCechy + '/'+ category  +addFilename + '.xml')

    # if modeCechy == "doc2vec":
    #     XMLPreparation.gensimClass.exampled2v(XMLPreparation.textArray,XMLPreparation.labelArray)



