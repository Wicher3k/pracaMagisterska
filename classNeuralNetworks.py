# # -*- coding: utf8 -*-
#
# import numpy as np
# from classLematization import LematizationClass
# from classesXMLLearning import XMLTokenizationBooleanWords
# from classArticle import Article
# from classMachineLearning import MachineLearning
# from keras.models import Sequential
# from keras.layers import Dense, Activation, Dropout
# from classGensim import GensimClass
# from f1metricNN import f1
#
# class NeuralNetworks(MachineLearning):
#
#     def __init__(self, categories, basefile, filenameFeatures="", extendedCSV=True, folder='', model='', onlySeparableCats=True, minAppearFeature=0, oneAcapit=False):
#         """ filenameFeatures potrzebny tylko dla worku"""
#         self.categories = categories
#         self.dictFeatures = {}
#         self.cechyArray = {}
#         self.lenFeatures = 0
#         self.basefile = basefile
#         self.filenameFeatures = filenameFeatures
#         self.extendedCSV = extendedCSV
#         self.subfolder = model
#         self.folder = folder
#         self.onlySeparableCats = onlySeparableCats
#         self.minAppearFeature = minAppearFeature
#         self.oneAcapit = oneAcapit
#         if oneAcapit:
#             self.oneAcapitFilename = "_oneAcapit"
#         else:
#             self.oneAcapitFilename = ""
#         if self.subfolder in ('word2vec'):
#             self.gensimClass = GensimClass(folder=self.folder, mode=self.subfolder, categories=self.categories,
#                                       oneAcapit=self.oneAcapit, minAppearFeature=minAppearFeature)
#             self.w2vmodel = None
#
#
#     def prepareBasesToFit(self):
#         idArraySepareteArticle = self._separeteArticlesMultiCategory()
#         if self.subfolder in ('lematyzacja','slowosiec',"lematyzacja+slowosiec"):
#             super(NeuralNetworks, self).prepereFeatures(self.categories, "NeuralNetwork")
#             XML = XMLTokenizationBooleanWords(self.folder, self.subfolder, self.cechyArray, idArraySepareteArticle, oneAcapit=self.oneAcapit)
#             XML.neuralNetworkVectors(self.categories)
#         elif self.subfolder in ('word2vec'):
#             self.w2vmodel = self.gensimClass.preparew2v(idArraySepareteArticle)
#
#     def neuralNetworksFit(self, raportOnly=False):
#
#         if not raportOnly:
#             print "neuralNetworksFit"
#
#             if self.cechyArray == {}:
#                 super(NeuralNetworks, self).importFeatures(self.filenameFeatures)
#
#             self.lenFeatures = len(self.cechyArray)
#             if self.subfolder in ('lematyzacja', 'slowosiec', "lematyzacja+slowosiec"):
#                 XML = XMLTokenizationBooleanWords(self.folder, self.subfolder, self.cechyArray, [], oneAcapit=self.oneAcapit)
#                 npFeaturesArray, npCategoriesArray = XML.neuralNetworkPrepereBase(self.categories)
#             elif self.subfolder in ('word2vec'):
#                 if self.w2vmodel is not None:
#                     npFeaturesArray, npCategoriesArray = self.gensimClass.avgw2v(self.w2vmodel)
#                 else:
#                     print "Brak w2vmodel"
#             print len(npCategoriesArray), self.lenFeatures
#             import time
#
#             start = time.time()
#             model = Sequential()
#             model.add(Dense(self.lenFeatures/100, input_dim=self.lenFeatures, kernel_initializer='uniform', activation='relu'))
#             model.add(Dropout(0.5))
#             # model.add(Dense(self.lenFeatures/8, kernel_initializer='uniform', activation='sigmoid', use_bias=True))
#             model.add(Dense(len(self.categories), kernel_initializer='uniform', activation='sigmoid'))
#
#             # Compile model
#             # model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
#             model.compile(loss='binary_crossentropy',
#                           optimizer='rmsprop',metrics=[f1])
#             # Fit the model
#             model.fit(npFeaturesArray, npCategoriesArray, epochs=20, batch_size=10, verbose=0, shuffle=True)
#             # evaluate the model
#             model.save_weights(self.basefile)
#             # wyswietla podsumowanie modelu
#             model.summary()
#             start2 = time.time()
#             print start2 - start
#
#     def neuralNetworksWork(self, objectTextsArray):
#         if self.cechyArray == {} or self.lenFeatures == 0:
#             super(NeuralNetworks, self).importFeatures(self.filenameFeatures)
#             self.lenFeatures = len(self.cechyArray)
#
#         XML = XMLTokenizationBooleanWords(self.folder, self.subfolder, self.cechyArray)
#         predictArray = XML.neuralNetworkPrepereObject(objectTextsArray)
#         print predictArray
#         model = Sequential()
#         model.add(Dense(self.lenFeatures/100, input_dim=self.lenFeatures, kernel_initializer='uniform', activation='relu'))
#         model.add(Dropout(0.5))
#         # model.add(Dense(self.lenFeatures / 3, kernel_initializer='uniform', activation='sigmoid', use_bias=True))
#         model.add(Dense(len(self.categories), kernel_initializer='uniform', activation='sigmoid'))
#
#         model.load_weights(self.basefile)
#
#         # print predictArray
#         predictions1 = model.predict(np.array(predictArray))
#         print predictions1
#
#
#     def featuresArray(self):
#         if self.cechyArray == []:
#             super(NeuralNetworks, self).importFeatures(self.filenameFeatures)
#         print self.cechyArray.index('biofizyka')
#         print self.cechyArray.index('cukrownictwo')
#         print self.cechyArray.index('curling')
#         print self.cechyArray.index('sport')
#         print self.cechyArray.index(u'kamień')
#         print self.cechyArray.index(u'zawodnik')
#         print self.cechyArray.index(u'sprzęt')
#         print self.cechyArray.index(u'szczotka')
#         print self.cechyArray.index(u'but')
#
#
# if __name__ == '__main__':
#     article = Article()
#     lematyzacjaClass = LematizationClass(False)
#     textsArray = []
#     #
#     # text = article.readText('resources/test2.docx')
#     # textsArray.append(lematyzacjaClass.lemmatization(lematyzacjaClass.removeChars(text)))
#     #
#     # text = article.readText('resources/test.docx')
#     # textsArray.append(lematyzacjaClass.lemmatization(lematyzacjaClass.removeChars(text)))
#
#     # categories = ['biofizyka', 'curling', 'cukrownictwo']
#     categories = ['curling']
#     categories = [u'biologia komórki', u'biologia molekularna', u'biologia rozrodu', u'statystyka opisowa',
#                   u'rośliny owocowe']
#     # categories = [u'biologia molekularna',u'statystyka opisowa',u'rośliny owocowe']
#     categories = [u'biologia komórki', u'biologia molekularna']
#     categories = [u'rośliny owocowe']
#     folder = '2018'
#     model = 'lematyzacja'
#     # model = 'slowosiec'
#     # model = "lematyzacja+slowosiec"
#     basefile = 'NN.h5'
#     filenameFeatures = 'features_NeuralNetwork_2018-4-12_10-44-54.txt' # nie musi byc podana tylko w przypadku gdy uzywamy juz worku
#     minAppearFeature = 2
#     oneAcapit = True
#     ML = NeuralNetworks(categories, basefile, filenameFeatures, True, folder, model, onlySeparableCats=True, minAppearFeature = minAppearFeature, oneAcapit = oneAcapit)
#     ML.prepareBasesToFit()
#     # ML.neuralNetworksFit()
#     textsArray.append(['truskawka','nazwa', 'naczyniowy', 'nasienny'])
#     # ML.neuralNetworksWork(textsArray)
